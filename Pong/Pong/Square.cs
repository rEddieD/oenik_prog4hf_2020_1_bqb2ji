﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong
{
    class Square : MyShape
    {
        static Random speed = new Random();
        public Square(double x, double y) : base(x, y, Config.BallSize, Config.BallSize)
        {
            Dx = 0;
            Dy = speed.Next(1, 4);
        }
    }
}
