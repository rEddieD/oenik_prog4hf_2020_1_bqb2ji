﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong
{
    class PongLogic
    {
        static Random R = new Random();
        static double MaximumX = Config.Width - Config.BallSize / 2;
        static double MinimumX = Config.BallSize / 2;
        PongModel model;

        public enum Direction { left, right };
        public event EventHandler RefreshScreen;

        public PongLogic(PongModel model)
        {
            this.model = model;
        }

        public void MovePad(Direction d)
        {
            if (d == Direction.left)
            {
                model.Pad.ChangeX(-10);
            }
            else
            {
                model.Pad.ChangeX(10);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void JumpPad(double x)
        {
            model.Pad.SetXY(x, model.Pad.Area.Y);
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private bool MoveShape(MyShape shape)
        {
            bool isFaulted = false;
            shape.ChangeX(shape.Dx);
            shape.ChangeY(shape.Dy);

            if (shape.Area.Left < 0 || shape.Area.Right > Config.Width)
            {
                shape.Dx = -shape.Dx;
            }
            if (shape.Area.Top < 0 || shape.Area.IntersectsWith(model.Pad.Area))
            {
                shape.Dy = -shape.Dy;
            }
            if (shape.Area.Bottom > Config.Height)
            {
                isFaulted = true;
                shape.SetXY(shape.Area.X, Config.Height / 2);
            }
            return isFaulted;
        }

        private bool SquareCollision(Square square)
        {
            if (model.Ball.Area.IntersectsWith(square.Area))
            {
                return true;
            }
            foreach (Star star in model.Stars)
            {
                if (star.Area.IntersectsWith(square.Area))

                {
                    return true;
                }
            }
            return false;
        }

        public void MoveBall()
        {
            if (MoveShape(model.Ball))
            {
                model.Errors++;
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void AddStar()
        {
            model.Stars.Add(new Star(Config.Width / 2, Config.Height / 2, Config.BallSize / 2, 6));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void MoveStar()
        {
            foreach (Star star in model.Stars)
            {
                if (MoveShape(star))
                {
                    model.Errors++;
                }
                RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        public void AddSquare()
        {
            while (model.Squares.Count < 3)
            {
                model.Squares.Add(new Square(R.NextDouble() * (MaximumX - MinimumX) + MinimumX, Config.BorderSize));
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }


        public void IsSquareCollided()
        {
            List<Square> collidedSquares = new List<Square>();
            foreach (Square square in model.Squares)
            {
                if (SquareCollision(square))
                {
                    collidedSquares.Add(square);
                    ChangeBallDirectionRandomly();
                }
            }
            foreach (Square square in collidedSquares)
            {
                model.Squares.Remove(square);
            }
            AddSquare();
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void MoveSquares()
        {
            foreach (Square square in model.Squares)
            {
                if (square.Area.Bottom > Config.Height / 2 || square.Area.Top < 0)
                {
                    square.Dy = -square.Dy;
                }
                square.ChangeY(square.Dy);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        
        public void ChangeBallDirectionRandomly()
        {
            model.Ball.Dx = R.Next(1, 10);
            if (R.Next(0,1) == 0)
            {
                model.Ball.Dx = -model.Ball.Dx;
            }
            //Dx can be everything beside 0
            model.Ball.Dy = 10 - Math.Abs(model.Ball.Dx);
        }
    }
}
