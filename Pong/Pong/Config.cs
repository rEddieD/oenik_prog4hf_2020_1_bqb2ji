﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Pong
{
    public static class Config
    {
        public static Brush BorderBrush = Brushes.White;
        public static Brush BgBrush = Brushes.Purple;

        public static Brush BallBgBrush = Brushes.White;
        public static Brush BallLineBrush = Brushes.Red;

        public static Brush PadBgBrush = Brushes.White;
        public static Brush PadlLineBrush = Brushes.Red;

        public static Brush SquareBgBrush = Brushes.Blue;
        public static Brush SquareLineBrush = Brushes.White;

        public static double Width = 700;
        public static double Height = 300;
        public static int BorderSize = 4;

        public static int BallSize = 20;
        public static int PadWidth = 100;
        public static int PadHeight = 20;
    }
}
