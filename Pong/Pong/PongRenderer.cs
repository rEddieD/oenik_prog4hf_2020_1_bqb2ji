﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Pong
{
    class PongRenderer
    {
        PongModel model;

        public PongRenderer(PongModel model)
        {
            this.model = model;
        }

        public void DrawThings(DrawingContext ctx)
        {
            DrawingGroup dg = new DrawingGroup();

            GeometryDrawing background = new GeometryDrawing(Config.BgBrush, new Pen(Config.BorderBrush, Config.BorderSize), new RectangleGeometry(new System.Windows.Rect(0, 0, Config.Width, Config.Height)));
            GeometryDrawing ball = new GeometryDrawing(Config.BallBgBrush, new Pen(Config.BallLineBrush, 1), new EllipseGeometry(model.Ball.Area));
            GeometryDrawing pad = new GeometryDrawing(Config.PadBgBrush, new Pen(Config.PadlLineBrush, 1), new RectangleGeometry(model.Pad.Area));
            FormattedText formattedText = new FormattedText(model.Errors.ToString(), System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.Black);
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red, 2), formattedText.BuildGeometry(new System.Windows.Point(5,5)));

            dg.Children.Add(background);
            dg.Children.Add(ball);
            dg.Children.Add(pad);
            dg.Children.Add(text);

            foreach(Star star in model.Stars)
            {
                GeometryDrawing stargeo = new GeometryDrawing(Config.BallBgBrush, new Pen(Config.BallLineBrush, 1), star.GetGeometry());
                dg.Children.Add(stargeo);
            }

            foreach(Square square in model.Squares)
            {
                GeometryDrawing squaregeo = new GeometryDrawing(Config.SquareBgBrush, new Pen(Config.SquareLineBrush, 1), new RectangleGeometry(square.Area));
                dg.Children.Add(squaregeo);
            }

            ctx.DrawDrawing(dg);
        }
    }
}
