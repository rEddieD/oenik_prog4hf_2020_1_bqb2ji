﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Input;
using System.Windows.Media;

namespace Pong
{
    class PongControl : FrameworkElement
    {
        PongModel model;
        PongLogic logic;
        PongRenderer renderer;
        DispatcherTimer tickTimer;

        public PongControl()
        {
            Loaded += PongControl_Loaded;
        }

        private void PongControl_Loaded(object sender, RoutedEventArgs e)
        {
            model = new PongModel();
            logic = new PongLogic(model);
            renderer = new PongRenderer(model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                tickTimer = new DispatcherTimer();
                tickTimer.Interval = TimeSpan.FromMilliseconds(40);
                tickTimer.Tick += TickTimer_Tick;
                tickTimer.Start();

                win.KeyDown += Win_KeyDown;
                win.MouseLeftButtonDown += Win_MouseLeftButtonDown;
            }
            logic.RefreshScreen += (obj, args) => InvalidateVisual();
            InvalidateVisual();
        }

        private void Win_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            logic.JumpPad(e.GetPosition(this).X);
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Left:
                    logic.MovePad(PongLogic.Direction.left); 
                    break;
                case Key.Right:
                    logic.MovePad(PongLogic.Direction.right);
                    break;
                case Key.Space:
                    logic.AddStar();
                    break;
            }
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            logic.MoveBall();
            logic.MoveStar();
            logic.IsSquareCollided();
            logic.MoveSquares();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null)
            {
                renderer.DrawThings(drawingContext);
            }
        }
    }
}
