﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Formula1.Data;
    using Formula1.Logic;
    using Formula1.Repository;

    /// <summary>
    /// Main class of the console program.
    /// </summary>
    public static class Program
    {
        private static void Main()
        {
            Console.CursorVisible = false;
            MainMenu();
            Console.WriteLine("Program has exited.");
            Console.ReadLine();
        }

        private static void MainMenu()
        {
            ConsoleKeyInfo key = default;
            while (key.Key != ConsoleKey.Escape)
            {
                MenuPrint(OperationSelection());
                int returned = MenuSelect(OperationSelection(), key);
                int ret;
                switch (returned)
                {
                    case 0:
                        do
                        {
                            MenuPrint(TableSelection());
                            ret = MenuSelect(TableSelection(), key);
                            ListAll(ret, key);
                        }
                        while (ret != -1);
                        break;
                    case 1:
                        do
                        {
                            MenuPrint(TableSelection());
                            ret = MenuSelect(TableSelection(), key);
                            GetOne(ret, key);
                        }
                        while (ret != -1);
                        break;
                    case 2:
                        do
                        {
                            ret = JoinOne();
                        }
                        while (ret != -1);
                        break;
                    case 3:
                        do
                        {
                            ret = JoinTeamOne();
                        }
                        while (ret != -1);
                        break;
                    case 4:
                        do
                        {
                            ret = GetAveragePoints();
                        }
                        while (ret != -1);
                        break;
                    case 5:
                        do
                        {
                            MenuPrint(TableSelection());
                            ret = MenuSelect(TableSelection(), key);
                            CreateOne(ret);
                        }
                        while (ret != -1);
                        break;
                    case 6:
                        do
                        {
                            MenuPrint(TableSelection());
                            ret = MenuSelect(TableSelection(), key);
                            RemoveOne(ret, key);
                        }
                        while (ret != -1);
                        break;
                    case 7:
                        do
                        {
                            MenuPrint(TableSelection());
                            ret = MenuSelect(TableSelection(), key);
                            UpdateOne(ret, key);
                        }
                        while (ret != -1);
                        break;
                    case 8:
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("HOME PROJECT 2019 - BQB2JI");
                        Console.WriteLine("|Java:");
                        Console.WriteLine("|----------------");
                        JavaLogic javlog = new JavaLogic(int.Parse(Console.ReadLine()));
                        var q = javlog.GetString();
                        foreach (var item in q)
                        {
                            Console.WriteLine("ID: " + item.ID);
                            Console.WriteLine("Name: " + item.Name);
                            Console.WriteLine("Value: " + item.Value);
                        }

                        key = Console.ReadKey();

                        break;
                    default:
                        return;
                }
            }
        }

        private static void ListAll(int which, ConsoleKeyInfo key)
        {
            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");
            switch (which)
            {
                case 0:
                    PilotLogic pl = new PilotLogic();
                    newList.Add("|Pilots:");
                    newList.Add("|----------------");
                    var q0 = pl.GetAllPilots();
                    string pilotLine = string.Empty;
                    foreach (var pilot in q0)
                    {
                        foreach (var property in pilot)
                        {
                            pilotLine += property + " ";
                        }

                        newList.Add(pilotLine);
                        pilotLine = string.Empty;
                    }

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 1:
                    CircuitLogic cl = new CircuitLogic();
                    newList.Add("|Circuits:");
                    newList.Add("|----------------");
                    var q1 = cl.GetAllCircuits();
                    string circuitLine = string.Empty;
                    foreach (var circuit in q1)
                    {
                        foreach (var property in circuit)
                        {
                            circuitLine += property + " ";
                        }

                        newList.Add(circuitLine);
                        circuitLine = string.Empty;
                    }

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 2:
                    TeamLogic tl = new TeamLogic();
                    newList.Add("|Teams:");
                    newList.Add("|----------------");
                    var q2 = tl.GetAllTeams();
                    string teamLine = string.Empty;
                    foreach (var team in q2)
                    {
                        foreach (var property in team)
                        {
                            teamLine += property + " ";
                        }

                        newList.Add(teamLine);
                        teamLine = string.Empty;
                    }

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 3:
                    OldRecordsLogic rl = new OldRecordsLogic();
                    newList.Add("|Records:");
                    newList.Add("|----------------");
                    var q3 = rl.GetAllRecords();
                    string recordLine = string.Empty;
                    foreach (var record in q3)
                    {
                        foreach (var property in record)
                        {
                            recordLine += property + " ";
                        }

                        newList.Add(recordLine);
                        recordLine = string.Empty;
                    }

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                default:
                    break;
            }
        }

        private static void GetOne(int c, ConsoleKeyInfo key)
        {
            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");
            int iD;
            switch (c)
            {
                case 0:
                    PilotLogic pl = new PilotLogic();
                    newList.Add("|Pilot:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q0 = pl.GetOnePilot(iD);
                    string pilot = string.Empty;
                    foreach (var property in q0)
                    {
                        pilot += property + " ";
                    }

                    newList.Add(pilot);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 1:
                    CircuitLogic cl = new CircuitLogic();
                    newList.Add("|Circuit:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q1 = cl.GetOneCircuit(iD);
                    string circuit = string.Empty;
                    foreach (var property in q1)
                    {
                        circuit += property + " ";
                    }

                    newList.Add(circuit);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 2:
                    TeamLogic tl = new TeamLogic();
                    newList.Add("|Team:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q2 = tl.GetOneTeam(iD);
                    string team = string.Empty;
                    foreach (var property in q2)
                    {
                        team += property + " ";
                    }

                    newList.Add(team);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 3:
                    OldRecordsLogic rl = new OldRecordsLogic();
                    newList.Add("|Record:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q3 = rl.GetOneRecord(iD);
                    string record = string.Empty;
                    foreach (var property in q3)
                    {
                        record += property + " ";
                    }

                    newList.Add(record);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                default:
                    break;
            }
        }

        private static void RemoveOne(int c, ConsoleKeyInfo key)
        {
            PilotLogic pl = new PilotLogic();
            CircuitLogic cl = new CircuitLogic();
            TeamLogic tl = new TeamLogic();
            OldRecordsLogic rl = new OldRecordsLogic();
            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");
            int iD;
            switch (c)
            {
                case 0:
                    newList.Add("|Pilot:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q0 = pl.GetOnePilot(iD);
                    string pilot = string.Empty;
                    foreach (var property in q0)
                    {
                        pilot += property + " ";
                    }

                    var circuitq = cl.GetAllCircuits();
                    foreach (var item in circuitq)
                    {
                        int tracknumber = 1;
                        if ((item[6] != string.Empty) && (int.Parse(item[6]) == iD))
                        {
                            var woncircuit = cl.GetOneCircuit(tracknumber);
                            woncircuit[6] = string.Empty;

                            // woncircuit[7] = string.Empty;
                            cl.UpdateCircuit(tracknumber, woncircuit);
                            Console.ReadLine();
                        }

                        tracknumber++;
                    }

                    newList.Add(pilot);
                    Console.Write("Successfully removed: " + pilot);
                    pl.RemovePilot(iD);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 1:
                    newList.Add("|Circuit:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q1 = cl.GetOneCircuit(iD);
                    string circuit = string.Empty;
                    foreach (var property in q1)
                    {
                        circuit += property + " ";
                    }

                    newList.Add(circuit);
                    Console.Write("Successfully removed: " + circuit);
                    cl.RemoveCircuit(iD);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 2:
                    newList.Add("|Team:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q2 = tl.GetOneTeam(iD);
                    string team = string.Empty;
                    foreach (var property in q2)
                    {
                        team += property + " ";
                    }

                    newList.Add(team);
                    Console.Write("Successfully removed: " + team);
                    tl.RemoveTeam(iD);

                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                case 3:
                    newList.Add("|Record:");
                    newList.Add("|----------------");
                    Console.Write("Enter ID: ");
                    iD = int.Parse(Console.ReadLine());
                    var q3 = rl.GetOneRecord(iD);
                    string record = string.Empty;
                    foreach (var property in q3)
                    {
                        record += property + " ";
                    }

                    newList.Add(record);
                    Console.Write("Successfully removed: " + record);
                    rl.RemoveRecord(iD);
                    MenuPrint(newList);
                    MenuSelect(newList, key);
                    newList.Clear();
                    break;
                default:
                    break;
            }
        }

        private static void UpdateOne(int c, ConsoleKeyInfo key)
        {
            PilotLogic pl = new PilotLogic();
            CircuitLogic cl = new CircuitLogic();
            TeamLogic tl = new TeamLogic();
            OldRecordsLogic rl = new OldRecordsLogic();

            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");

            int returned;

            switch (c)
            {
                case 0:
                    newList.Add("|Pilots:");
                    newList.Add("|----------------");
                    var q0 = pl.GetAllPilots();
                    string pilotLine = string.Empty;
                    foreach (var pilot in q0)
                    {
                        foreach (var property in pilot)
                        {
                            pilotLine += property + " ";
                        }

                        newList.Add(pilotLine);
                        pilotLine = string.Empty;
                    }

                    MenuPrint(newList);
                    returned = MenuSelect(newList, key);
                    Console.Clear();
                    Console.WriteLine(newList[returned + 3]);
                    Console.ReadLine();
                    List<string> pilotAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Birth: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Nationality: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Points: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Number of races won: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Team ID: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    var smackthat = newList[returned + 3].Split(' ');
                    pl.UpdatePilot(int.Parse(smackthat[0]), pilotAddList);
                    newList.Clear();
                    break;
                case 1:
                    newList.Add("|Circuits:");
                    newList.Add("|----------------");
                    var q1 = cl.GetAllCircuits();
                    string circuitLine = string.Empty;
                    foreach (var circuit in q1)
                    {
                        foreach (var property in circuit)
                        {
                            circuitLine += property + " ";
                        }

                        newList.Add(circuitLine);
                        circuitLine = string.Empty;
                    }

                    MenuPrint(newList);
                    returned = MenuSelect(newList, key);
                    Console.Clear();
                    Console.WriteLine(newList[returned + 3]);
                    Console.ReadLine();
                    List<string> circuitAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Date: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Direction: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Laps: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Distance: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Pilot that won: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Pilot that holds the record: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    var smackthatt = newList[returned + 3].Split(' ');
                    cl.UpdateCircuit(int.Parse(smackthatt[0]), circuitAddList);
                    newList.Clear();
                    break;
                case 2:
                    newList.Add("|Teams:");
                    newList.Add("|----------------");
                    var q2 = tl.GetAllTeams();
                    string teamLine = string.Empty;
                    foreach (var team in q2)
                    {
                        foreach (var property in team)
                        {
                            teamLine += property + " ";
                        }

                        newList.Add(teamLine);
                        teamLine = string.Empty;
                    }

                    MenuPrint(newList);
                    returned = MenuSelect(newList, key);
                    Console.Clear();
                    Console.WriteLine(newList[returned + 3]);
                    Console.ReadLine();
                    List<string> teamsAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Nationality: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Team chief: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Power unit: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Number of employees: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Budget: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    var ssmaackthatt = newList[returned + 3].Split(' ');
                    tl.UpdateTeam(int.Parse(ssmaackthatt[0]), teamsAddList);
                    newList.Clear();
                    break;
                case 3:
                    newList.Add("|Records:");
                    newList.Add("|----------------");
                    var q3 = rl.GetAllRecords();
                    string recordLine = string.Empty;
                    foreach (var record in q3)
                    {
                        foreach (var property in record)
                        {
                            recordLine += property + " ";
                        }

                        newList.Add(recordLine);
                        recordLine = string.Empty;
                    }

                    MenuPrint(newList);
                    returned = MenuSelect(newList, key);
                    Console.Clear();
                    Console.WriteLine(newList[returned + 3]);
                    Console.ReadLine();
                    List<string> recordsAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Date: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Direction: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Laps: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Distance: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Pilot that won: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Pilot that holds the record: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    var ssmackthatt = newList[returned + 3].Split(' ');
                    rl.UpdateRecord(int.Parse(ssmackthatt[0]), recordsAddList);
                    newList.Clear();
                    break;
                default:
                    break;
            }
        }

        private static void CreateOne(int c)
        {
            PilotLogic pl = new PilotLogic();
            CircuitLogic cl = new CircuitLogic();
            TeamLogic tl = new TeamLogic();
            OldRecordsLogic rl = new OldRecordsLogic();

            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");

            switch (c)
            {
                case 0:
                    newList.Add("|Pilots:");
                    newList.Add("|----------------");
                    var q0 = pl.GetAllPilots();
                    string pilotLine = string.Empty;
                    foreach (var pilot in q0)
                    {
                        foreach (var property in pilot)
                        {
                            pilotLine += property + " ";
                        }

                        newList.Add(pilotLine);
                        pilotLine = string.Empty;
                    }

                    MenuPrint(newList);
                    Console.ReadLine();
                    List<string> pilotAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Birth: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Nationality: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Points: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Number of races won: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    Console.WriteLine("Team ID: ");
                    NotNullReadLine(Console.ReadLine(), pilotAddList);
                    pl.CreatePilot(pilotAddList);
                    newList.Clear();
                    break;
                case 1:
                    newList.Add("|Circuits:");
                    newList.Add("|----------------");
                    var q1 = cl.GetAllCircuits();
                    string circuitLine = string.Empty;
                    foreach (var circuit in q1)
                    {
                        foreach (var property in circuit)
                        {
                            circuitLine += property + " ";
                        }

                        newList.Add(circuitLine);
                        circuitLine = string.Empty;
                    }

                    MenuPrint(newList);
                    Console.ReadLine();
                    List<string> circuitAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Date: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Direction: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Laps: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Distance: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Pilot that won: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    Console.WriteLine("Pilot that holds the record: ");
                    NotNullReadLine(Console.ReadLine(), circuitAddList);
                    cl.CreateCircuit(circuitAddList);
                    newList.Clear();
                    break;
                case 2:
                    newList.Add("|Teams:");
                    newList.Add("|----------------");
                    var q2 = tl.GetAllTeams();
                    string teamLine = string.Empty;
                    foreach (var team in q2)
                    {
                        foreach (var property in team)
                        {
                            teamLine += property + " ";
                        }

                        newList.Add(teamLine);
                        teamLine = string.Empty;
                    }

                    MenuPrint(newList);
                    Console.ReadLine();
                    List<string> teamsAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Nationality: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Team chief: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Power unit: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Number of employees: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    Console.WriteLine("Budget: ");
                    NotNullReadLine(Console.ReadLine(), teamsAddList);
                    tl.CreateTeam(teamsAddList);
                    newList.Clear();
                    break;
                case 3:
                    newList.Add("|Records:");
                    newList.Add("|----------------");
                    var q3 = rl.GetAllRecords();
                    string recordLine = string.Empty;
                    foreach (var record in q3)
                    {
                        foreach (var property in record)
                        {
                            recordLine += property + " ";
                        }

                        newList.Add(recordLine);
                        recordLine = string.Empty;
                    }

                    MenuPrint(newList);
                    Console.ReadLine();
                    List<string> recordsAddList = new List<string>();
                    Console.WriteLine("ID: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Name: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Year: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    Console.WriteLine("Record time: ");
                    NotNullReadLine(Console.ReadLine(), recordsAddList);
                    rl.CreateRecord(recordsAddList);
                    newList.Clear();
                    break;
                default:
                    break;
            }
        }

        private static void NotNullReadLine(string readLine, List<string> listToAdd)
        {
            if (readLine.Length != 0)
            {
                listToAdd.Add(readLine);
            }
            else
            {
                throw new InvalidCastException();
            }
        }

        private static int MenuPrint(IList<string> menuList, int selected = 3)
        {
            Console.ForegroundColor = ConsoleColor.White;
            string linesToPrint = BuildString(menuList as List<string>);
            Clear(0, 0, 120, 25);
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(linesToPrint);
            Console.SetCursorPosition(0, selected);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(menuList[selected]);
            return selected;
        }

        private static int JoinOne()
        {
            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");
            JoinLogic jl = new JoinLogic();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            var q = jl.JoinWinCircuitTables();
            foreach (var item in q)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
            return -1;
        }

        private static int JoinTeamOne()
        {
            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");
            JoinLogic jl = new JoinLogic();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            var q = jl.WhoIsInWhatTeam();
            foreach (var item in q)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
            return -1;
        }

        private static int GetAveragePoints()
        {
            List<string> newList = new List<string>();
            newList.Add("HOME PROJECT 2019 - BQB2JI");
            JoinLogic jl = new JoinLogic();
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            var q = jl.GetAveragePoints();
            Console.WriteLine(q);
            Console.ReadLine();
            return -1;
        }

        private static int MenuSelect(IList<string> menuList, ConsoleKeyInfo key)
        {
            int start = 3;
            int limit = menuList.Count - start;

            while (key.Key != ConsoleKey.Escape)
            {
                key = Console.ReadKey(true);
                int pos;
                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        pos = (((Console.CursorTop - start) + 1 + limit) % limit) + start;
                        MenuPrint(menuList, pos);
                        break;
                    case ConsoleKey.UpArrow:
                        pos = (((Console.CursorTop - start) - 1 + limit) % limit) + start;
                        MenuPrint(menuList, pos);
                        break;
                    case ConsoleKey.Enter:
                        return Console.CursorTop - start;
                    default:
                        break;
                }
            }

            return -1;
        }

        private static List<string> OperationSelection()
        {
            List<string> opList = new List<string>
            {
                "HOME PROJECT 2019 - BQB2JI",
                "|Operations:",
                "|-----------------",
                "|1. List all",
                "|2. Get one",
                "|3. Join who won what",
                "|4. Join who is in team",
                "|5. Get average points",
                "|6. Create",
                "|7. Delete",
                "|8. Update",
                "|9. Java",
            };

            return opList;
        }

        private static List<string> TableSelection()
        {
            List<string> tableMenuList = new List<string>
            {
                "HOME PROJECT 2019 - BQB2JI",
                "|Tables:",
                "|----------------",
                "|1. Pilots",
                "|2. Circuits",
                "|3. Teams",
                "|4. Records",
            };

            return tableMenuList;
        }

        private static string BuildString(List<string> input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Count; i++)
            {
                sb.Append(input[i] + "\n");
            }

            return sb.ToString();
        }

        private static void Clear(int x, int y, int width, int height)
        {
            int curTop = Console.CursorTop;
            int curLeft = Console.CursorLeft;
            for (; height > 0;)
            {
                Console.SetCursorPosition(x, y + --height);
                Console.Write(new string(' ', width));
            }

            Console.SetCursorPosition(curLeft, curTop);
        }
    }
}
