var class_formula1_1_1_logic_1_1_test_1_1_tests =
[
    [ "TestGetAllCircuits", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a1276c5bbbd09ddf7ad68c0becfab4d64", null ],
    [ "TestGetAllPilots", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#ab30623d6e6b7d4f8408ffa90408cbe9a", null ],
    [ "TestGetAllRecords", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#ad0e0cafb4ea957048f601cb7e13ae561", null ],
    [ "TestGetAllTeams", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#ab36c14320e8582006d5d02db50ea450e", null ],
    [ "TestGetAveragePoints", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a777fac58a96d864ccd994e0b2d24bc91", null ],
    [ "TestGetOneCircuit", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#aa8bca2bd5a73bb43798ac2d4d16fa46f", null ],
    [ "TestGetOnePilot", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#ab068c56d29ac3db04d2cfa8b00c9fa4a", null ],
    [ "TestGetOneRecord", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a0f3467763ce0e97cb5caffa63920d2a8", null ],
    [ "TestGetOneTeam", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a5e1de7733e1a0d2d263292fbbd84dc4e", null ],
    [ "TestJoinPilotCircuit", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#aac3e5406408656d602b6e706e2bb9692", null ],
    [ "TestJoinPilotTeam", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a64ec3f758c1771e807346f11d2b1aac8", null ],
    [ "TestRemoveOneCircuit", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a0169ed10e02adc992e27654c48a70980", null ],
    [ "TestRemoveOnePilot", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#aeee09f8a06a5dd40a7d335939c1afb89", null ],
    [ "TestRemoveOneRecord", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a6f0a750a7ed9bc853c358b8fcb122378", null ],
    [ "TestRemoveOneTeam", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a9152167be08a84ed598cc9541e80ae01", null ],
    [ "TestsSetup", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a05f7f018d6dec461dbedb7733bb367fa", null ],
    [ "TestUpdateOneCircuit", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a89739bf93b2af524cf09b237862ee9a0", null ],
    [ "TestUpdateOnePilot", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#aee25022e644d8e95b3b8c0fc4cc4a07b", null ],
    [ "TestUpdateOneRecord", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a6a665805189109fad3dfc0206dafc3fa", null ],
    [ "TestUpdateOneTeam", "class_formula1_1_1_logic_1_1_test_1_1_tests.html#a49f1a9004254a8133797fc1615baf6a8", null ]
];