var class_formula1_1_1_repository_1_1_repository =
[
    [ "Repository", "class_formula1_1_1_repository_1_1_repository.html#a7d8cf7e72b968d5a48cba05b8742b28f", null ],
    [ "AddOne", "class_formula1_1_1_repository_1_1_repository.html#afa6c1b7d2fd28ab4c00c8f5b9049c395", null ],
    [ "GetAll", "class_formula1_1_1_repository_1_1_repository.html#a6c336d461741b5a6a5bdbd4d6ee3843f", null ],
    [ "GetOne", "class_formula1_1_1_repository_1_1_repository.html#ad2ebec533d896ff3567a07956c246fad", null ],
    [ "RemoveOne", "class_formula1_1_1_repository_1_1_repository.html#a4d952cc36a0d699a3c6fa8f436bf70d6", null ],
    [ "UpdateOne", "class_formula1_1_1_repository_1_1_repository.html#a7f34103da2fd6bb7c748a62128ef401a", null ],
    [ "Context", "class_formula1_1_1_repository_1_1_repository.html#a5659e324da225b0b33cad1f1a83baabb", null ]
];