var class_formula1_1_1_data_1_1_circuits =
[
    [ "Circuits_dateOfRace", "class_formula1_1_1_data_1_1_circuits.html#a8450b6a644e4861d634c690535ed61d2", null ],
    [ "Circuits_direction", "class_formula1_1_1_data_1_1_circuits.html#a1738cf7d2451305c6b5ef13596462f0d", null ],
    [ "Circuits_distance", "class_formula1_1_1_data_1_1_circuits.html#a29c43478034e10ce7fb3f7b1d82e3429", null ],
    [ "Circuits_id", "class_formula1_1_1_data_1_1_circuits.html#a07c68c886d5ef027b69087c636491a74", null ],
    [ "Circuits_laps", "class_formula1_1_1_data_1_1_circuits.html#a5b225039e8336bcda6d596b1aff0c2dd", null ],
    [ "Circuits_name", "class_formula1_1_1_data_1_1_circuits.html#aa746b6d8635eee7082fe675f04e2cbf0", null ],
    [ "Circuits_pilotRecord", "class_formula1_1_1_data_1_1_circuits.html#a17ce8a4f5626e3b060b162d1bf52b03a", null ],
    [ "Circuits_pilotWon", "class_formula1_1_1_data_1_1_circuits.html#adc7f25e5cae6dbb1a0478be2ef81e3d0", null ],
    [ "OldRecords", "class_formula1_1_1_data_1_1_circuits.html#a58201b047a2b831444b3b48c83a1d15c", null ],
    [ "Pilots", "class_formula1_1_1_data_1_1_circuits.html#a5639aaad5cae6d1fbaf0624ce758f3fd", null ]
];