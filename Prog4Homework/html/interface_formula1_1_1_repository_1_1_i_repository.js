var interface_formula1_1_1_repository_1_1_i_repository =
[
    [ "AddOne", "interface_formula1_1_1_repository_1_1_i_repository.html#a28b9e8433a951e601a96ff0bcca00dbb", null ],
    [ "GetAll", "interface_formula1_1_1_repository_1_1_i_repository.html#afaeda8486bad69857ff39c1e677ecef3", null ],
    [ "GetOne", "interface_formula1_1_1_repository_1_1_i_repository.html#aabfa4043bc91caf1ba461f965b436332", null ],
    [ "RemoveOne", "interface_formula1_1_1_repository_1_1_i_repository.html#ae222d4693e172d9428eee82d8f19a4dc", null ],
    [ "UpdateOne", "interface_formula1_1_1_repository_1_1_i_repository.html#aef77445ed20fe9b3d73813b46ef3285b", null ]
];