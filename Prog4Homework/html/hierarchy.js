var hierarchy =
[
    [ "Formula1.Logic.CircuitLogic", "class_formula1_1_1_logic_1_1_circuit_logic.html", null ],
    [ "Formula1.Data.Circuits", "class_formula1_1_1_data_1_1_circuits.html", null ],
    [ "DbContext", null, [
      [ "Formula1.Data::Formula1DatabaseEntities", "class_formula1_1_1_data_1_1_formula1_database_entities.html", null ]
    ] ],
    [ "Formula1.Logic.IPilotLogic", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html", [
      [ "Formula1.Logic.PilotLogic", "class_formula1_1_1_logic_1_1_pilot_logic.html", null ]
    ] ],
    [ "Formula1.Repository.IRepository< T >", "interface_formula1_1_1_repository_1_1_i_repository.html", [
      [ "Formula1.Repository.Repository< T >", "class_formula1_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "Formula1.Repository.IRepository< Circuits >", "interface_formula1_1_1_repository_1_1_i_repository.html", [
      [ "Formula1.Repository.ICircuitRepository", "interface_formula1_1_1_repository_1_1_i_circuit_repository.html", [
        [ "Formula1.Repository.CircuitRepository", "class_formula1_1_1_repository_1_1_circuit_repository.html", null ]
      ] ]
    ] ],
    [ "Formula1.Repository.IRepository< OldRecords >", "interface_formula1_1_1_repository_1_1_i_repository.html", [
      [ "Formula1.Repository.IOldRecordsRepository", "interface_formula1_1_1_repository_1_1_i_old_records_repository.html", [
        [ "Formula1.Repository.OldRecordsRepository", "class_formula1_1_1_repository_1_1_old_records_repository.html", null ]
      ] ]
    ] ],
    [ "Formula1.Repository.IRepository< Pilots >", "interface_formula1_1_1_repository_1_1_i_repository.html", [
      [ "Formula1.Repository.IPilotRepository", "interface_formula1_1_1_repository_1_1_i_pilot_repository.html", [
        [ "Formula1.Repository.PilotRepository", "class_formula1_1_1_repository_1_1_pilot_repository.html", null ]
      ] ]
    ] ],
    [ "Formula1.Repository.IRepository< Teams >", "interface_formula1_1_1_repository_1_1_i_repository.html", [
      [ "Formula1.Repository.ITeamRepository", "interface_formula1_1_1_repository_1_1_i_team_repository.html", [
        [ "Formula1.Repository.TeamRepository", "class_formula1_1_1_repository_1_1_team_repository.html", null ]
      ] ]
    ] ],
    [ "Formula1.Logic.JavaLogic", "class_formula1_1_1_logic_1_1_java_logic.html", null ],
    [ "Formula1.Logic.JoinLogic", "class_formula1_1_1_logic_1_1_join_logic.html", null ],
    [ "Formula1.Data.OldRecords", "class_formula1_1_1_data_1_1_old_records.html", null ],
    [ "Formula1.Logic.OldRecordsLogic", "class_formula1_1_1_logic_1_1_old_records_logic.html", null ],
    [ "Formula1.Logic.Person", "class_formula1_1_1_logic_1_1_person.html", null ],
    [ "Formula1.Data.Pilots", "class_formula1_1_1_data_1_1_pilots.html", null ],
    [ "Formula1.Repository.Repository< Circuits >", "class_formula1_1_1_repository_1_1_repository.html", [
      [ "Formula1.Repository.CircuitRepository", "class_formula1_1_1_repository_1_1_circuit_repository.html", null ]
    ] ],
    [ "Formula1.Repository.Repository< OldRecords >", "class_formula1_1_1_repository_1_1_repository.html", [
      [ "Formula1.Repository.OldRecordsRepository", "class_formula1_1_1_repository_1_1_old_records_repository.html", null ]
    ] ],
    [ "Formula1.Repository.Repository< Pilots >", "class_formula1_1_1_repository_1_1_repository.html", [
      [ "Formula1.Repository.PilotRepository", "class_formula1_1_1_repository_1_1_pilot_repository.html", null ]
    ] ],
    [ "Formula1.Repository.Repository< Teams >", "class_formula1_1_1_repository_1_1_repository.html", [
      [ "Formula1.Repository.TeamRepository", "class_formula1_1_1_repository_1_1_team_repository.html", null ]
    ] ],
    [ "Formula1.Logic.TeamLogic", "class_formula1_1_1_logic_1_1_team_logic.html", null ],
    [ "Formula1.Data.Teams", "class_formula1_1_1_data_1_1_teams.html", null ],
    [ "Formula1.Logic.Test.Tests", "class_formula1_1_1_logic_1_1_test_1_1_tests.html", null ]
];