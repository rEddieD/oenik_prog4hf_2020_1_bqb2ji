var class_formula1_1_1_logic_1_1_team_logic =
[
    [ "TeamLogic", "class_formula1_1_1_logic_1_1_team_logic.html#a58bfea66a01a63d2223d85377b17d968", null ],
    [ "TeamLogic", "class_formula1_1_1_logic_1_1_team_logic.html#a1cffaf1a654b8041b5c7f583370fc575", null ],
    [ "CreateTeam", "class_formula1_1_1_logic_1_1_team_logic.html#a66aca1ad57a1a88fd60499d6a81ceb07", null ],
    [ "GetAllTeams", "class_formula1_1_1_logic_1_1_team_logic.html#a90215f91040e03d29332cd9b3f2e514c", null ],
    [ "GetOneTeam", "class_formula1_1_1_logic_1_1_team_logic.html#aa53896210893452c3566d39ae3ef9c75", null ],
    [ "RemoveTeam", "class_formula1_1_1_logic_1_1_team_logic.html#abe2c49c4b804e8294e95e0ffeaacbe9f", null ],
    [ "UpdateTeam", "class_formula1_1_1_logic_1_1_team_logic.html#aba2a11b8ee5270795b38aba726e06a57", null ]
];