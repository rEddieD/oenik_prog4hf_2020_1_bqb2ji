var class_formula1_1_1_logic_1_1_pilot_logic =
[
    [ "PilotLogic", "class_formula1_1_1_logic_1_1_pilot_logic.html#abe59d97b04b3ad770aa6efeba0af49b9", null ],
    [ "PilotLogic", "class_formula1_1_1_logic_1_1_pilot_logic.html#afe9ca5a86f77eb70595d2dab708f38d2", null ],
    [ "ChangePoint", "class_formula1_1_1_logic_1_1_pilot_logic.html#a857d1ae7a2f2a3d849c17dea83357afb", null ],
    [ "ChangeTeam", "class_formula1_1_1_logic_1_1_pilot_logic.html#ab267489e569951b3e126638d2933745b", null ],
    [ "CreatePilot", "class_formula1_1_1_logic_1_1_pilot_logic.html#a38aea2c90fe6e2f20165ddd0f304b968", null ],
    [ "GetAllPilots", "class_formula1_1_1_logic_1_1_pilot_logic.html#aae059a1563de923ba8376bf03b1584ed", null ],
    [ "GetOnePilot", "class_formula1_1_1_logic_1_1_pilot_logic.html#a8fb3df0c8194bc63850fec22fa188e14", null ],
    [ "RemovePilot", "class_formula1_1_1_logic_1_1_pilot_logic.html#aca0ee6e2adab55846c486b1f554bfd73", null ],
    [ "UpdatePilot", "class_formula1_1_1_logic_1_1_pilot_logic.html#a12eadd1385933b2d31ca62bbecd75940", null ]
];