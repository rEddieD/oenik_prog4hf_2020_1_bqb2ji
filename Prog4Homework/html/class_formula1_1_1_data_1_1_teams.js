var class_formula1_1_1_data_1_1_teams =
[
    [ "Teams", "class_formula1_1_1_data_1_1_teams.html#a85f8ca65cbde44242fe37ab9e3a6d41d", null ],
    [ "Pilots", "class_formula1_1_1_data_1_1_teams.html#adbb34b403b04743406a6e50f5f069a36", null ],
    [ "team_budget", "class_formula1_1_1_data_1_1_teams.html#a6d6e1e212749297ca28fb517e9826993", null ],
    [ "team_employees", "class_formula1_1_1_data_1_1_teams.html#ab86ca76a6c9009c8e4153cc479fb9bfa", null ],
    [ "team_id", "class_formula1_1_1_data_1_1_teams.html#a1177612fd0df274130d2769ed7daf800", null ],
    [ "team_name", "class_formula1_1_1_data_1_1_teams.html#afb0f899f930630247a4fe92404b904a6", null ],
    [ "team_nationality", "class_formula1_1_1_data_1_1_teams.html#a3343a8d7c925ecc478ee69a530902566", null ],
    [ "team_powerUnit", "class_formula1_1_1_data_1_1_teams.html#a9e7bcd4e1a691527d477e1b8e461bea1", null ],
    [ "team_teamChief", "class_formula1_1_1_data_1_1_teams.html#a41f756f105d63ada916dd53ff5398894", null ]
];