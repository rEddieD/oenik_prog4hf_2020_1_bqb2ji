var namespaces_dup =
[
    [ "NUnit 3.10.1 - March 12, 2018", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md34", null ],
    [ "NUnit 3.10 - March 12, 2018", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md35", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md33", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md36", null ]
    ] ],
    [ "NUnit 3.9 - November 10, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md37", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md38", null ]
    ] ],
    [ "NUnit 3.8.1 - August 28, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md39", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md40", null ]
    ] ],
    [ "NUnit 3.8 - August 27, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md41", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md42", null ]
    ] ],
    [ "NUnit 3.7.1 - June 6, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md43", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md44", null ]
    ] ],
    [ "NUnit 3.7 - May 29, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md45", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md46", null ]
    ] ],
    [ "NUnit 3.6.1 - February 26, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md47", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md48", null ]
    ] ],
    [ "NUnit 3.6 - January 9, 2017", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md49", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md50", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md51", null ]
    ] ],
    [ "NUnit 3.5 - October 3, 2016", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md52", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md53", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md54", null ]
    ] ],
    [ "NUnit 3.4.1 - June 30, 2016", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md55", [
      [ "Console Runner", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md56", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md57", null ]
    ] ],
    [ "NUnit 3.4 - June 25, 2016", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md58", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md59", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md60", null ],
      [ "Console Runner", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md61", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md62", null ]
    ] ],
    [ "NUnit 3.2.1 - April 19, 2016", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md63", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md64", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md65", null ],
      [ "Console Runner", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md66", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md67", null ]
    ] ],
    [ "NUnit 3.2 - March 5, 2016", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md68", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md69", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md70", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md71", null ]
    ] ],
    [ "NUnit 3.0.1 - December 1, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md72", [
      [ "Console Runner", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md73", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md74", null ]
    ] ],
    [ "NUnit 3.0.0 Final Release - November 15, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md75", [
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md76", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 3 - November 13, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md77", [
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md78", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md79", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate 2 - November 8, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md80", [
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md81", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md82", null ]
    ] ],
    [ "NUnit 3.0.0 Release Candidate - November 1, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md83", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md84", null ],
      [ "NUnitLite", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md85", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md86", null ],
      [ "Console Runner", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md87", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md88", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 5 - October 16, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md89", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md90", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md91", null ],
      [ "Console Runner", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md92", [
        [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md93", null ]
      ] ]
    ] ],
    [ "NUnit 3.0.0 Beta 4 - August 25, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md94", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md95", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md96", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md97", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 3 - July 15, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md98", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md99", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md100", null ],
      [ "Console", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md101", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md102", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 2 - May 12, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md103", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md104", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md105", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md106", null ]
    ] ],
    [ "NUnit 3.0.0 Beta 1 - March 25, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md107", [
      [ "General", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md108", null ],
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md109", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md110", null ],
      [ "Console", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md111", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md112", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 5 - January 30, 2015", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md113", [
      [ "General", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md114", null ],
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md115", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md116", null ],
      [ "Console", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md117", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md118", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 4 - December 30, 2014", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md119", [
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md120", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md121", null ],
      [ "Console", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md122", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md123", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 3 - November 29, 2014", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md124", [
      [ "Breaking Changes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md125", null ],
      [ "Framework", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md126", null ],
      [ "Engine", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md127", null ],
      [ "Console", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md128", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md129", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 2 - November 2, 2014", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md130", [
      [ "Breaking Changes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md131", null ],
      [ "General", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md132", null ],
      [ "New Features", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md133", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md134", null ]
    ] ],
    [ "NUnit 3.0.0 Alpha 1 - September 22, 2014", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md135", [
      [ "Breaking Changes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md136", null ],
      [ "General", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md137", null ],
      [ "New Features", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md138", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md139", null ],
      [ "Console Issues Resolved (Old nunit-console project, now combined with nunit)", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md140", null ]
    ] ],
    [ "NUnit 2.9.7 - August 8, 2014", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md141", [
      [ "Breaking Changes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md142", null ],
      [ "New Features", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md143", null ],
      [ "Issues Resolved", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md144", null ]
    ] ],
    [ "NUnit 2.9.6 - October 4, 2013", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md145", [
      [ "Main Features", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md146", null ],
      [ "Bug Fixes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md147", null ],
      [ "Bug Fixes in 2.9.6 But Not Listed Here in the Release", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md148", null ]
    ] ],
    [ "NUnit 2.9.5 - July 30, 2010", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md149", [
      [ "Bug Fixes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md150", null ]
    ] ],
    [ "NUnit 2.9.4 - May 4, 2010", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md151", [
      [ "Bug Fixes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md152", null ]
    ] ],
    [ "NUnit 2.9.3 - October 26, 2009", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md153", [
      [ "Main Features", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md154", null ],
      [ "Bug Fixes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md155", null ]
    ] ],
    [ "NUnit 2.9.2 - September 19, 2009", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md156", [
      [ "Main Features", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md157", null ],
      [ "Bug Fixes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md158", null ]
    ] ],
    [ "NUnit 2.9.1 - August 27, 2009", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md159", [
      [ "General", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md160", null ],
      [ "Bug Fixes", "md__g_1__project_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__b_q_b2_j_i_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html#autotoc_md161", null ]
    ] ],
    [ "Formula1", "namespace_formula1.html", "namespace_formula1" ]
];