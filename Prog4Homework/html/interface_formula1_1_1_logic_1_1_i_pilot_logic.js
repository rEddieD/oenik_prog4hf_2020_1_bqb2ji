var interface_formula1_1_1_logic_1_1_i_pilot_logic =
[
    [ "ChangePoint", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#a5d07f6c56f1e8490053689b93bf88a69", null ],
    [ "ChangeTeam", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#aca649a89f80cfc3fe41a5ab83d1ab25a", null ],
    [ "CreatePilot", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#aff2d5637f567d0268d5a306cd4e51ef7", null ],
    [ "GetAllPilots", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#aa74f7e6ae1ac35a575b8aac620c19876", null ],
    [ "GetOnePilot", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#a9a685ab4d51fbf1aca76cff68b805f35", null ],
    [ "RemovePilot", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#a1ad87177e277eadfa3603481d0ee14b3", null ],
    [ "UpdatePilot", "interface_formula1_1_1_logic_1_1_i_pilot_logic.html#a1303eb90504babd4fac8ccd29a48828d", null ]
];