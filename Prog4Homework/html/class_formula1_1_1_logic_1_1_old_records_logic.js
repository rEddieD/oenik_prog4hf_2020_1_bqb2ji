var class_formula1_1_1_logic_1_1_old_records_logic =
[
    [ "OldRecordsLogic", "class_formula1_1_1_logic_1_1_old_records_logic.html#aea5c94307725e724c8307e3aa19d4a28", null ],
    [ "OldRecordsLogic", "class_formula1_1_1_logic_1_1_old_records_logic.html#a289c7030dbca8bb48d6b6d1cb1a63b78", null ],
    [ "CreateRecord", "class_formula1_1_1_logic_1_1_old_records_logic.html#a1c557b4246cc50129107188b135eb294", null ],
    [ "GetAllRecords", "class_formula1_1_1_logic_1_1_old_records_logic.html#a53d3aaf5c629bbbaa575cfd7339bdc5d", null ],
    [ "GetOneRecord", "class_formula1_1_1_logic_1_1_old_records_logic.html#a584f28c0edd11af577f6ba71e89cf61b", null ],
    [ "RemoveRecord", "class_formula1_1_1_logic_1_1_old_records_logic.html#a247bfb56640b7ab7d36d5af55cc0bc9a", null ],
    [ "UpdateRecord", "class_formula1_1_1_logic_1_1_old_records_logic.html#a908f7c030caaa78c3275d2a17b821ea0", null ]
];