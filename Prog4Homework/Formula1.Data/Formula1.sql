IF object_id('Circuits', 'U') IS NOT NULL DROP TABLE Circuits;
IF object_id('OldRecords', 'U') IS NOT NULL DROP TABLE OldRecords;
IF object_id('Pilots', 'U') IS NOT NULL DROP TABLE Pilots;
IF object_id('Teams', 'U') IS NOT NULL DROP TABLE Teams;

GO

CREATE TABLE Teams(
				team_id int identity primary key,
				team_name nvarchar(100),
				team_teamChief nvarchar(100),
				team_powerUnit nvarchar(100),
				team_budget int,
				team_employees int,
				team_nationality nvarchar(100)
				);

CREATE TABLE Pilots(
				pilot_id int primary key,
				pilot_name nvarchar (100),
				pilot_birth datetime2,
				pilot_racesWon int,
				pilot_points int,
				pilot_nationality nvarchar(100),
				pilot_team int references Teams(team_id)
				);
				
CREATE TABLE OldRecords(
				OldRecords_id int primary key,
				OldRecords_name nvarchar(100),
				OldRecords_year int,
				OldRecords_recordTime time,
				);

CREATE TABLE Circuits(
				Circuits_id int identity primary key,
				Circuits_name nvarchar(250),
				Circuits_distance float(7),
				Circuits_pilotRecord int unique references OldRecords(OldRecords_id),
				Circuits_direction nvarchar(20),
				Circuits_laps int,
				Circuits_dateOfRace datetime2,
				Circuits_pilotWon int references Pilots(pilot_id)
				);
GO

INSERT INTO Teams VALUES ('Scuderia Ferrari', 'Maurizio Arrivabene', 'Ferrari', 410, 950, 'Italy');
INSERT INTO Teams VALUES ('Mercedes AMG Petronas Motorsport', 'Toto Wolff', 'Mercedes', 400, 950, 'Germany');
INSERT INTO Teams VALUES ('Aston Martin Red Bull Racing', 'Christian Horner', 'Honda', 310, 860, 'Austria');
INSERT INTO Teams VALUES ('Renault Sport Formula One Team', 'Cyril Abiteboul', 'Renault', 190, 625, 'France');
INSERT INTO Teams VALUES ('Haas F1 Team', 'Guenther Steiner', 'Ferrari', 130, 250, 'America');
INSERT INTO Teams VALUES ('McLaren F1 Team', 'Zak Brown', 'Renault', 220, 760, 'United Kingdom');
INSERT INTO Teams VALUES ('Racing Point Force India F1 Team', 'Otmar Szafnauer', 'Mercedes', 120, 405, 'United Kingdom');
INSERT INTO Teams VALUES ('Red Bull Toro Rosso Honda', 'Franz Tost', 'Honda', 150, 460, 'Italy');
INSERT INTO Teams VALUES ('Alfa Romeo Sauber F1 Team', 'Frédéric Vasseur', 'Ferrari', 135, 400, 'Switzerland');
INSERT INTO Teams VALUES ('Williams Martini Racing', 'Paddy Lowe', 'Mercedes', 150, 630, 'United Kingdom');

INSERT INTO Pilots VALUES (5, 'Sebastian Vettel', '1987-07-03', 53, 230, 'Germany', 1);
INSERT INTO Pilots VALUES (16, 'Charles Leclerc', '1997-10-16', 2, 236, 'Monaco', 1);
INSERT INTO Pilots VALUES (44, 'Lewis Hamilton', '1985-01-07', 83, 363, 'United Kingdom', 2);
INSERT INTO Pilots VALUES (77, 'Valtteri Bottas', '1989-08-28', 6, 289, 'Finland', 2);
INSERT INTO Pilots VALUES (23, 'Alexander Albon', '1996-03-23', 0, 74, 'Thailand', 3);
INSERT INTO Pilots VALUES (33, 'Max Verstappen', '1997-09-30', 7, 220, 'Netherlands', 3);
INSERT INTO Pilots VALUES (3, 'Daniel Ricciardo', '1989-07-01', 7, 38, 'Australia', 4);
INSERT INTO Pilots VALUES (27, 'Nico Hulkenberg', '1987-08-19', 0, 35, 'Germany', 4);
INSERT INTO Pilots VALUES (8, 'Romain Grosjean', '1986-04-17', 0, 8, 'France', 5);
INSERT INTO Pilots VALUES (20, 'Kevin Magnussen', '1992-10-05', 0, 20, 'Denmark', 5);
INSERT INTO Pilots VALUES (4, 'Lando Norris', '1999-11-13', 0, 35, 'United Kingdom', 6);
INSERT INTO Pilots VALUES (55, 'Carlos Sainz', '1994-09-01', 0, 76, 'Spain', 6);
INSERT INTO Pilots VALUES (11, 'Sergio Perez', '1990-01-26', 0, 43, 'Mexico', 7);
INSERT INTO Pilots VALUES (18, 'Lance Stroll', '1998-10-29', 0, 21, 'Canada', 7);
INSERT INTO Pilots VALUES (10, 'Pierre Gasly', '1996-02-07', 0, 77, 'France', 8);
INSERT INTO Pilots VALUES (26, 'Daniil Kvyat', '1994-04-26', 0, 34, 'Russian Federation', 8);
INSERT INTO Pilots VALUES (7, 'Kimi Räikkönen', '1979-10-17', 21, 31, 'Finland', 9);
INSERT INTO Pilots VALUES (99, 'Antonio Giovinazzi', '1993-12-14', 0, 4, 'Italy', 9);
INSERT INTO Pilots VALUES (63, 'George Russell', '1998-02-15', 0, 0, 'United Kingdom', 10);
INSERT INTO Pilots VALUES (88, 'Robert Kubica', '1984-12-07', 1, 1, 'Poland', 10);

INSERT INTO OldRecords VALUES (1, 'Michael Schumacher', 2004, '00:1:24.125');
INSERT INTO OldRecords VALUES (2, 'Pedro de la Rosa', 2005, '00:1:31.447');
INSERT INTO OldRecords VALUES (3, 'Michael Schumacher', 2004, '00:1:32.238');
INSERT INTO OldRecords VALUES (4, 'Charles Leclerc', 2019, '00:1:43.009');
INSERT INTO OldRecords VALUES (5, 'Daniel Ricciardo', 2018, '00:1:18.441');
INSERT INTO OldRecords VALUES (6, 'Max Verstappen', 2018, '00:1:14.260');
INSERT INTO OldRecords VALUES (7, 'Valtteri Bottas', 2019, '00:1:13.078');
INSERT INTO OldRecords VALUES (8, 'Sebastian Vettel', 2019, '00:1:32.740');
INSERT INTO OldRecords VALUES (9, 'Kimi Räikkönen', 2018, '00:1:06.957');
INSERT INTO OldRecords VALUES (10, 'Lewis Hamilton', 2019, '00:1:27.369');
INSERT INTO OldRecords VALUES (11, 'Kimi Räikkönen', 2004, '00:1:13.780');
INSERT INTO OldRecords VALUES (12, 'Max Verstappen', 2019, '00:1:17.103');
INSERT INTO OldRecords VALUES (13, 'Valtteri Bottas', 2018, '00:1:46.286');
INSERT INTO OldRecords VALUES (14, 'Rubens Barrichello', 2004, '00:1:21.046');
INSERT INTO OldRecords VALUES (15, 'Kevin Magnussen', 2018, '00:1:41.905');
INSERT INTO OldRecords VALUES (16, 'Lewis Hamilton', 2019, '00:1:35.761');
INSERT INTO OldRecords VALUES (17, 'Lewis Hamilton', 2019, '00:1:30.983');
INSERT INTO OldRecords VALUES (18, 'Valtteri Bottas', 2018, '00:1:18.741');
INSERT INTO OldRecords VALUES (19, 'Lewis Hamilton', 2018, '00:1:37.392');
INSERT INTO OldRecords VALUES (20, 'Valtteri Bottas', 2018, '00:1:10.540');
INSERT INTO OldRecords VALUES (21, 'Sebastian Vettel', 2009, '00:1:40.279');

INSERT INTO Circuits VALUES ('Albert Park Circuit - Australian Grand Prix 2019', 5.303, 1, 'Clockwise', 58, '2019-03-17', 77);
INSERT INTO Circuits VALUES ('Bahrain International Circuit - Bahrain Grand Prix 2019', 5.412, 2, 'Clockwise', 57, '2019-03-31', 44);
INSERT INTO Circuits VALUES ('Shanghai International Circuit - Chinese Grand Prix 2019', 5.451, 3, 'Clockwise', 56, '2019-04-14', 44);
INSERT INTO Circuits VALUES ('Baku City Circuit - Azerbaijan Grand Prix 2019', 6.003, 4, 'Counter-clockwise', 51, '2019-04-28', 77);
INSERT INTO Circuits VALUES ('Circuit de Catalunya - Spanish Grand Prix 2019', 4.655, 5, 'Clockwise', 66, '2019-05-12', 44);
INSERT INTO Circuits VALUES ('Circuit de Monaco - Monaco Grand Prix 2019', 3.337, 6, 'Clockwise', 78, '2019-05-26', 44);
INSERT INTO Circuits VALUES ('Circuit Gilles Villeneuve - Canadian Grand Prix 2019', 4.361, 7, 'Clockwise', 70, '2019-06-9', 44);
INSERT INTO Circuits VALUES ('Circuit Paul Ricard - French Grand Prix 2019', 5.842, 8, 'Clockwise', 53, '2019-06-23', 44);
INSERT INTO Circuits VALUES ('Red Bull Ring - Austrian Grand Prix 2019', 4.318, 9, 'Clockwise', 71, '2019-06-30', 33);
INSERT INTO Circuits VALUES ('Silverstone Circuit - British Grand Prix 2019', 5.891, 10, 'Clockwise', 52, '2019-07-14', 44);
INSERT INTO Circuits VALUES ('Hockenheimring - German Grand Prix 2019', 4.574, 11, 'Clockwise', 67, '2019-07-22', 33);
INSERT INTO Circuits VALUES ('Hungaroring - Hungarian Grand Prix 2019', 4.381, 12, 'Clockwise', 70, '2019-08-04', 44);
INSERT INTO Circuits VALUES ('Circuit de Spa-Francorchamps - Belgian Grand Prix 2019', 7.004, 13, 'Clockwise', 44, '2019-09-01', 16);
INSERT INTO Circuits VALUES ('Autodromo di Monza - Italian Grand Prix 2019', 5.793, 14, 'Clockwise', 53, '2019-09-08', 16);
INSERT INTO Circuits VALUES ('Marina Bay Street Circuit - Singapore Grand Prix 2019', 5.063, 15, 'Counter-clockwise', 61, '2019-09-22', 5);
INSERT INTO Circuits VALUES ('Sochi Autodrom - Russian Grand Prix 2019', 5.848, 16, 'Clockwise', 53, '2019-09-29', 44);
INSERT INTO Circuits VALUES ('Suzuka Circuit - Japanese Grand Prix 2019', 5.807, 17, 'Both', 53, '2019-10-13', 77);
INSERT INTO Circuits VALUES ('Autodromo Hermanos Rodriguez - Mexican Grand Prix 2019', 4.304, 18, 'Clockwise', 71, '2019-10-27', NULL);
INSERT INTO Circuits VALUES ('Circuit of the Americas - United States Grand Prix 2019', 5.513, 19, 'Counter-clockwise', 56, '2019-11-03', NULL);
INSERT INTO Circuits VALUES ('Interlagos Circuit - Brazilian Grand Prix 2019', 4.309, 20, 'Counter-clockwise', 71, '2019-11-17', NULL);
INSERT INTO Circuits VALUES ('Yas Marina Circuit - Abu Dhabi Grand Prix 2019', 5.554, 21, 'Counter-clockwise', 55, '2019-12-01', NULL);
