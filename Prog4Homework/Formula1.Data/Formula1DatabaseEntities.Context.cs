﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Formula1.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Formula1DatabaseEntities : DbContext
    {
        public Formula1DatabaseEntities()
            : base("name=Formula1DatabaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Circuits> Circuits { get; set; }
        public virtual DbSet<OldRecords> OldRecords { get; set; }
        public virtual DbSet<Pilots> Pilots { get; set; }
        public virtual DbSet<Teams> Teams { get; set; }
    }
}
