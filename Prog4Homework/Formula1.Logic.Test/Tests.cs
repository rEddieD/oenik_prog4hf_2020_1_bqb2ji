﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Formula1.Data;
    using Formula1.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests of the logic class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        private Mock<IPilotRepository> mockedPilot;
        private Mock<ICircuitRepository> mockedCircuit;
        private Mock<ITeamRepository> mockedTeam;
        private Mock<IOldRecordsRepository> mockedRecords;

        private PilotLogic pl;
        private CircuitLogic cl;
        private TeamLogic tl;
        private OldRecordsLogic rl;

        private JoinLogic jl;

        private List<Pilots> pList;
        private List<Circuits> cList;
        private List<Teams> tList;
        private List<OldRecords> rList;

        /// <summary>
        /// Setup for tests.
        /// </summary>
        [SetUp]
        public void TestsSetup()
        {
            this.mockedPilot = new Mock<IPilotRepository>();
            this.mockedCircuit = new Mock<ICircuitRepository>();
            this.mockedTeam = new Mock<ITeamRepository>();
            this.mockedRecords = new Mock<IOldRecordsRepository>();

            this.pList = new List<Pilots>()
            {
                new Pilots() { pilot_id = 1, pilot_name = "Tök Mag", pilot_birth = DateTime.Parse("1994.02.25"), pilot_nationality = "HUN", pilot_points = 142, pilot_racesWon = 10, pilot_team = 1 },
                new Pilots() { pilot_id = 2, pilot_name = "Végh Béla", pilot_birth = DateTime.Parse("1984.02.25"), pilot_nationality = "SWE", pilot_points = 55, pilot_racesWon = 0, pilot_team = 2 },
                new Pilots() { pilot_id = 7, pilot_name = "Cserepes Virág", pilot_birth = DateTime.Parse("1995.01.11"), pilot_nationality = "USA", pilot_points = 22, pilot_racesWon = 0, pilot_team = 2 },
                new Pilots() { pilot_id = 14, pilot_name = "Balatoni Napsugár", pilot_birth = DateTime.Parse("1999.10.15"), pilot_nationality = "UK", pilot_points = 75, pilot_racesWon = 3, pilot_team = 3 },
            };

            this.cList = new List<Circuits>()
            {
                new Circuits() { Circuits_id = 1, Circuits_name = "Grandekurva", Circuits_dateOfRace = DateTime.Parse("1989.02.25"), Circuits_laps = 50, Circuits_distance = 5.303f, Circuits_direction = "BOTH", Circuits_pilotWon = 1, Circuits_pilotRecord = 1 },
                new Circuits() { Circuits_id = 2, Circuits_name = "Multicurve", Circuits_dateOfRace = DateTime.Parse("1954.05.01"), Circuits_laps = 82, Circuits_distance = 6.189f, Circuits_direction = "COUNTER-CLOCKWISE", Circuits_pilotWon = 1, Circuits_pilotRecord = 3 },
                new Circuits() { Circuits_id = 3, Circuits_name = "Wheellooser", Circuits_dateOfRace = DateTime.Parse("1994.07.30"), Circuits_laps = 66, Circuits_distance = 3.499f, Circuits_direction = "COUNTER-CLOCKWISE", Circuits_pilotWon = 2, Circuits_pilotRecord = 3 },
                new Circuits() { Circuits_id = 4, Circuits_name = "OnlyBernie", Circuits_dateOfRace = DateTime.Parse("1974.08.16"), Circuits_laps = 45, Circuits_distance = 2.555f, Circuits_direction = "CLOCKWISE", Circuits_pilotWon = 1, Circuits_pilotRecord = 4 },
            };

            this.tList = new List<Teams>()
            {
                new Teams() { team_id = 1, team_name = "BMW", team_budget = 4000, team_nationality = "GER", team_employees = 5000, team_powerUnit = "BMW", team_teamChief = "Piros Pista" },
                new Teams() { team_id = 2, team_name = "Red Point", team_budget = 400, team_nationality = "USA", team_employees = 300, team_powerUnit = "Renault", team_teamChief = "Poros Pista" },
                new Teams() { team_id = 3, team_name = "LOL", team_budget = 2500, team_nationality = "ITA", team_employees = 3500, team_powerUnit = "Mercedes", team_teamChief = "Víz Elek" },
                new Teams() { team_id = 4, team_name = "Ferrari", team_budget = 303, team_nationality = "HUN", team_employees = 2222, team_powerUnit = "Ferrari", team_teamChief = "Tűz Elek" },
            };

            this.rList = new List<OldRecords>()
            {
                new OldRecords() { OldRecords_id = 1, OldRecords_name = "Tök Mag", OldRecords_year = 2010, OldRecords_recordTime = TimeSpan.ParseExact("01:25.240", @"mm\:ss\.fff", CultureInfo.CurrentCulture) },
                new OldRecords() { OldRecords_id = 2, OldRecords_name = "Tök Mag", OldRecords_year = 2008, OldRecords_recordTime = TimeSpan.ParseExact("01:44.030", @"mm\:ss\.fff", CultureInfo.CurrentCulture) },
                new OldRecords() { OldRecords_id = 3, OldRecords_name = "Tök Mag", OldRecords_year = 2009, OldRecords_recordTime = TimeSpan.ParseExact("00:59.033", @"mm\:ss\.fff", CultureInfo.CurrentCulture) },
                new OldRecords() { OldRecords_id = 4, OldRecords_name = "Végh Béla", OldRecords_year = 2011, OldRecords_recordTime = TimeSpan.ParseExact("01:12.087", @"mm\:ss\.fff", CultureInfo.CurrentCulture) },
            };

            this.mockedPilot.Setup(repo => repo.GetAll()).Returns(this.pList.AsQueryable());
            this.mockedCircuit.Setup(repo => repo.GetAll()).Returns(this.cList.AsQueryable());
            this.mockedTeam.Setup(repo => repo.GetAll()).Returns(this.tList.AsQueryable());
            this.mockedRecords.Setup(repo => repo.GetAll()).Returns(this.rList.AsQueryable());

            this.mockedPilot.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns((int x) => this.pList[x - 1]);
            this.mockedCircuit.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns((int x) => this.cList[x - 1]);
            this.mockedTeam.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns((int x) => this.tList[x - 1]);
            this.mockedRecords.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns((int x) => this.rList[x - 1]);

            this.mockedPilot.Setup(repo => repo.RemoveOne(It.IsAny<Pilots>())).Callback((Pilots x) => this.pList.Remove(x));
            this.mockedCircuit.Setup(repo => repo.RemoveOne(It.IsAny<Circuits>())).Callback((Circuits x) => this.cList.Remove(x));
            this.mockedTeam.Setup(repo => repo.RemoveOne(It.IsAny<Teams>())).Callback((Teams x) => this.tList.Remove(x));
            this.mockedRecords.Setup(repo => repo.RemoveOne(It.IsAny<OldRecords>())).Callback((OldRecords x) => this.rList.Remove(x));

            this.mockedPilot.Setup(repo => repo.UpdateOne(It.IsAny<Pilots>(), It.IsAny<Pilots>())).Callback((Pilots x, Pilots y) =>
            {
                int pos = this.pList.IndexOf(x);
                this.pList.Remove(x);
                this.pList.Insert(pos, y);
            });
            this.mockedCircuit.Setup(repo => repo.UpdateOne(It.IsAny<Circuits>(), It.IsAny<Circuits>())).Callback((Circuits x, Circuits y) =>
            {
                int pos = this.cList.IndexOf(x);
                this.cList.Remove(x);
                this.cList.Insert(pos, y);
            });
            this.mockedTeam.Setup(repo => repo.UpdateOne(It.IsAny<Teams>(), It.IsAny<Teams>())).Callback((Teams x, Teams y) =>
            {
                int pos = this.tList.IndexOf(x);
                this.tList.Remove(x);
                this.tList.Insert(pos, y);
            });
            this.mockedRecords.Setup(repo => repo.UpdateOne(It.IsAny<OldRecords>(), It.IsAny<OldRecords>())).Callback((OldRecords x, OldRecords y) =>
            {
                int pos = this.rList.IndexOf(x);
                this.rList.Remove(x);
                this.rList.Insert(pos, y);
            });

            this.pl = new PilotLogic(this.mockedPilot.Object);
            this.cl = new CircuitLogic(this.mockedCircuit.Object);
            this.tl = new TeamLogic(this.mockedTeam.Object);
            this.rl = new OldRecordsLogic(this.mockedRecords.Object);
            this.jl = new JoinLogic(this.mockedPilot.Object, this.mockedCircuit.Object, this.mockedTeam.Object, this.mockedRecords.Object);
        }

        /// <summary>
        /// Test for getting all pilots.
        /// </summary>
        [Test]
        public void TestGetAllPilots()
        {
            var result = this.pl.GetAllPilots();
            Assert.That(result.Count(), Is.EqualTo(4));
            this.mockedPilot.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test for getting all circuits.
        /// </summary>
        [Test]
        public void TestGetAllCircuits()
        {
            var result = this.cl.GetAllCircuits();
            Assert.That(result.Count(), Is.EqualTo(4));
            this.mockedCircuit.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test for getting all teams.
        /// </summary>
        [Test]
        public void TestGetAllTeams()
        {
            var result = this.tl.GetAllTeams();
            Assert.That(result.Count(), Is.EqualTo(4));
            this.mockedTeam.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test for getting all records.
        /// </summary>
        [Test]
        public void TestGetAllRecords()
        {
            var result = this.rl.GetAllRecords();
            Assert.That(result.Count(), Is.EqualTo(4));
            this.mockedRecords.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test for getting one pilot.
        /// </summary>
        [Test]
        public void TestGetOnePilot()
        {
            var result = this.pl.GetOnePilot(1);
            Assert.That(result, Is.TypeOf<List<string>>());
            this.mockedPilot.Verify(repo => repo.GetOne(1), Times.Once);
        }

        /// <summary>
        /// Test for getting one circuit.
        /// </summary>
        [Test]
        public void TestGetOneCircuit()
        {
            var result = this.cl.GetOneCircuit(1);
            Assert.That(result, Is.TypeOf<List<string>>());
            this.mockedCircuit.Verify(repo => repo.GetOne(1), Times.Once);
        }

        /// <summary>
        /// Test for getting one team.
        /// </summary>
        [Test]
        public void TestGetOneTeam()
        {
            var result = this.tl.GetOneTeam(1);
            Assert.That(result, Is.TypeOf<List<string>>());
            this.mockedTeam.Verify(repo => repo.GetOne(1), Times.Once);
        }

        /// <summary>
        /// Test for getting one record.
        /// </summary>
        [Test]
        public void TestGetOneRecord()
        {
            var result = this.rl.GetOneRecord(1);
            Assert.That(result, Is.TypeOf<List<string>>());
            this.mockedRecords.Verify(repo => repo.GetOne(1), Times.Once);
        }

        /// <summary>
        /// Test whether it removes a pilot.
        /// </summary>
        [Test]
        public void TestRemoveOnePilot()
        {
            Assert.That(this.pList.Count(), Is.EqualTo(4));
            this.pl.RemovePilot(1);
            Assert.That(this.pList.Count(), Is.EqualTo(3));
            this.mockedPilot.Verify(x => x.RemoveOne(It.IsAny<Pilots>()), Times.Once);
        }

        /// <summary>
        /// Test whether it removes a circuit.
        /// </summary>
        [Test]
        public void TestRemoveOneCircuit()
        {
            Assert.That(this.cList.Count(), Is.EqualTo(4));
            this.cl.RemoveCircuit(1);
            Assert.That(this.cList.Count(), Is.EqualTo(3));
            this.mockedCircuit.Verify(x => x.RemoveOne(It.IsAny<Circuits>()), Times.Once);
        }

        /// <summary>
        /// Test whether it removes a team.
        /// </summary>
        [Test]
        public void TestRemoveOneTeam()
        {
            Assert.That(this.tList.Count(), Is.EqualTo(4));
            this.tl.RemoveTeam(1);
            Assert.That(this.tList.Count(), Is.EqualTo(3));
            this.mockedTeam.Verify(x => x.RemoveOne(It.IsAny<Teams>()), Times.Once);
        }

        /// <summary>
        /// Test whether it removes a record.
        /// </summary>
        [Test]
        public void TestRemoveOneRecord()
        {
            Assert.That(this.rList.Count(), Is.EqualTo(4));
            this.rl.RemoveRecord(1);
            Assert.That(this.rList.Count(), Is.EqualTo(3));
            this.mockedRecords.Verify(x => x.RemoveOne(It.IsAny<OldRecords>()), Times.Once);
        }

        /// <summary>
        /// Test whether it updates a pilot.
        /// Order is important!!!
        /// ID
        /// NAME
        /// BIRTHDATE
        /// NATIONALITY
        /// POINTS
        /// RACESWON
        /// TEAM.
        /// </summary>
        [Test]
        public void TestUpdateOnePilot()
        {
            Assert.That(this.pList.Count(), Is.EqualTo(4));
            Assert.That(this.pList[1].pilot_nationality, Is.EqualTo("SWE"));
            this.pl.UpdatePilot(2, new List<string> { "2", "Végh Béla", "1984.02.25", "HUN", "55", "0", "2" });
            Assert.That(this.pList.Count(), Is.EqualTo(4));
            Assert.That(this.pList[1].pilot_nationality, Is.EqualTo("HUN"));
        }

        /// <summary>
        /// Test whether it updates a circuit.
        /// Order is important!!!
        /// ID
        /// NAME
        /// DATE OF RACE
        /// DIRECTION
        /// LAPS
        /// DISTANCE
        /// PILOT THAT WON
        /// PILOT THAT HOLDS RECORD ON THIS TRACK.
        /// </summary>
        [Test]
        public void TestUpdateOneCircuit()
        {
            Assert.That(this.cList.Count(), Is.EqualTo(4));
            Assert.That(this.cList[0].Circuits_laps, Is.EqualTo(50));
            Assert.That(this.cList[0].Circuits_name, Is.EqualTo("Grandekurva"));
            this.cl.UpdateCircuit(1, new List<string> { "1", "Hubibubi", "1989.2.25", "BOTH", "70", "5.303", "1", "1" });
            Assert.That(this.cList.Count(), Is.EqualTo(4));
            Assert.That(this.cList[0].Circuits_laps, Is.EqualTo(70));
            Assert.That(this.cList[0].Circuits_name, Is.EqualTo("Hubibubi"));
        }

        /// <summary>
        /// Test whether it updates a team.
        /// Order is important!!!
        /// ID
        /// NAME
        /// NATIONALITY
        /// TEAMCHIEF
        /// POWERUNIT
        /// EMPLOYEES
        /// BUDGET.
        /// </summary>
        [Test]
        public void TestUpdateOneTeam()
        {
            Assert.That(this.tList.Count(), Is.EqualTo(4));
            Assert.That(this.tList[3].team_nationality, Is.EqualTo("HUN"));
            this.tl.UpdateTeam(4, new List<string> { "4", "Ferrari", "ITA", "Tűz Elek", "Ferrari", "2222", "303" });
            Assert.That(this.tList.Count(), Is.EqualTo(4));
            Assert.That(this.tList[3].team_nationality, Is.EqualTo("ITA"));
        }

        /// <summary>
        /// Test whether it updates a record.
        /// Order is important!!!
        /// ID
        /// NAME
        /// YEAR
        /// RECORDTIME.
        /// </summary>
        [Test]
        public void TestUpdateOneRecord()
        {
            Assert.That(this.rList.Count(), Is.EqualTo(4));
            Assert.That(this.rList[1].OldRecords_year, Is.EqualTo(2008));
            this.rl.UpdateRecord(2, new List<string> { "2", "Madár", "2010", "01:43.030" });
            Assert.That(this.rList.Count(), Is.EqualTo(4));
            Assert.That(this.rList[1].OldRecords_year, Is.EqualTo(2010));
        }

        /// <summary>
        /// Test GetAveraPoints for getting the average point of pilots.
        /// </summary>
        [Test]
        public void TestGetAveragePoints()
        {
            var ave = this.jl.GetAveragePoints();
            int sum = 142 + 55 + 22 + 75;
            double average = sum / 4.0;
            Assert.That(ave, Is.EqualTo(average));
        }

        /// <summary>
        /// Test of joining table pilots with table circuits (LEFT JOIN).
        /// </summary>
        [Test]
        public void TestJoinPilotCircuit()
        {
            var all = this.jl.JoinWinCircuitTables();
            Assert.That(all.Count, Is.GreaterThan(0));
            Assert.That(all[0], Does.Contain("Tök Mag"));
            Assert.That(all[1], Does.Contain("Tök Mag"));
            Assert.That(all[0], Does.Contain("Grandekurva"));
            Assert.That(all[1], Does.Contain("Multicurve"));
            this.mockedTeam.Verify(x => x.GetAll(), Times.Never);
            this.mockedPilot.Verify(x => x.GetAll(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Test of joining table pilots with table teams (LEFT JOIN).
        /// </summary>
        [Test]
        public void TestJoinPilotTeam()
        {
            var all = this.jl.WhoIsInWhatTeam();
            Assert.That(all.Count, Is.GreaterThan(0));
            Assert.That(all[0], Does.Contain("Tök Mag"));
            Assert.That(all[1], Does.Contain("Végh Béla"));
            Assert.That(all[0], Does.Contain("BMW"));
            Assert.That(all[1], Does.Contain("Red Point"));
        }
    }
}
