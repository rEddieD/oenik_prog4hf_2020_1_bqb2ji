﻿// <copyright file="JoinLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Formula1.Data;
    using Formula1.Repository;

    /// <summary>
    /// Logic for joining tables.
    /// </summary>
    public class JoinLogic
    {
        private IPilotRepository pl;
        private ICircuitRepository cl;
        private ITeamRepository tl;
        private IOldRecordsRepository rl;

        /// <summary>
        /// Initializes a new instance of the <see cref="JoinLogic"/> class.
        /// </summary>
        public JoinLogic()
        {
            this.pl = new PilotRepository();
            this.cl = new CircuitRepository();
            this.tl = new TeamRepository();
            this.rl = new OldRecordsRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JoinLogic"/> class.
        /// For moq.
        /// </summary>
        /// <param name="pl">IPilotRepository.</param>
        /// <param name="cl">ICircuitRepository.</param>
        /// <param name="tl">ITeamRepository.</param>
        /// <param name="rl">IOldRecordsRepository.</param>
        public JoinLogic(IPilotRepository pl, ICircuitRepository cl, ITeamRepository tl, IOldRecordsRepository rl)
        {
            this.pl = pl;
            this.cl = cl;
            this.tl = tl;
            this.rl = rl;
        }

        /// <summary>
        /// Join tables.
        /// </summary>
        /// <returns>Returns joined list.</returns>
        public List<string> JoinWinCircuitTables()
        {
            var pilotQuery = this.pl.GetAll().AsEnumerable();
            var circuitQuery = this.cl.GetAll().AsEnumerable();

            var join = pilotQuery
                .GroupJoin(
                circuitQuery,
                pilot => pilot.pilot_id,
                circuit => circuit.Circuits_pilotWon,
                (pil, cir) => new { pil, cir }).SelectMany(
                x => x.cir.DefaultIfEmpty(),
                (pilot, circuit) => new
                {
                    pilot = pilot.pil.pilot_name,
                    circuit = circuit == null ? "NA" : circuit.Circuits_name,
                });
            List<string> joined = new List<string>();
            foreach (var item in join)
            {
                joined.Add($"Pilot won on this circuit: {item.pilot} - {item.circuit}");
            }

            return joined;
        }

        /// <summary>
        /// Who is in what team.
        /// </summary>
        /// <returns>Returns joined list.</returns>
        public List<string> WhoIsInWhatTeam()
        {
            var pilotQuery = this.pl.GetAll().AsEnumerable();
            var teamQuery = this.tl.GetAll().AsEnumerable();

            var join = pilotQuery
                .Join(
                    teamQuery,
                    pilot => pilot.pilot_team,
                    team => team.team_id,
                    (pilot, team) => new
                    {
                        pilot = pilot.pilot_name,
                        team = team.team_name,
                    }).ToList();

            List<string> joined = new List<string>();
            foreach (var item in join)
            {
                joined.Add($"Pilot and its team: {item.pilot}, {item.team}");
            }

            return joined;
        }

        /// <summary>
        /// Gets the average points of the pilots.
        /// </summary>
        /// <returns>Returns the average.</returns>
        public double GetAveragePoints()
        {
            var pilotQuery = this.pl.GetAll().AsEnumerable();
            var average = pilotQuery.Average(x => x.pilot_points);
            double d = average.Value;
            return d;
        }
    }
}
