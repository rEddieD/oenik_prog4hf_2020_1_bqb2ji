﻿// <copyright file="CircuitLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Formula1.Data;
    using Formula1.Repository;

    /// <summary>
    /// Business class for Pilots table.
    /// </summary>
    public class CircuitLogic
    {
        private ICircuitRepository circuitRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitLogic"/> class.
        /// </summary>
        public CircuitLogic()
        {
            this.circuitRepo = new CircuitRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitLogic"/> class.
        /// </summary>
        /// <param name="repo">Pilot repository.</param>
        public CircuitLogic(ICircuitRepository repo)
        {
            this.circuitRepo = repo;
        }

        /// <summary>
        /// Get all the pilots in the table.
        /// </summary>
        /// <returns>Returns all the pilots in table.</returns>
        public List<List<string>> GetAllCircuits()
        {
            var allCircuits = this.circuitRepo.GetAll();
            List<List<string>> ret = new List<List<string>>(allCircuits.AsEnumerable().Select(
                x => new List<string>()
                {
                    x.Circuits_id.ToString(),
                    x.Circuits_name,
                    x.Circuits_dateOfRace.ToString(),
                    x.Circuits_direction,
                    x.Circuits_laps.ToString(),
                    x.Circuits_distance.ToString(),
                    x.Circuits_pilotWon.ToString(),
                    x.Circuits_pilotRecord.ToString(),
                }));
            return ret;
        }

        /// <summary>
        /// Get one pilot.
        /// </summary>
        /// <param name="id">ID of the pilot.</param>
        /// <returns>Returns the pilot with the ID.</returns>
        public List<string> GetOneCircuit(int id)
        {
            var oneCircuit = this.circuitRepo.GetOne(id);
            List<string> ret = new List<string>()
            {
                oneCircuit.Circuits_id.ToString(),
                oneCircuit.Circuits_name,
                oneCircuit.Circuits_dateOfRace.ToString(),
                oneCircuit.Circuits_direction,
                oneCircuit.Circuits_laps.ToString(),
                oneCircuit.Circuits_distance.ToString(),
                oneCircuit.Circuits_pilotWon.ToString(),
                oneCircuit.Circuits_pilotRecord.ToString(),
            };
            return ret;
        }

        /// <summary>
        /// Creating a pilot object and passing it to the repository.
        /// </summary>
        /// <param name="properties">List of properties a pilot has.</param>
        public void CreateCircuit(IList<string> properties)
        {
            Circuits c = new Circuits();

            int.TryParse(properties[0], out int id);
            c.Circuits_id = id;

            c.Circuits_name = properties[1];

            DateTime.TryParseExact(properties[2], "yyyy.M.d", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime date);
            c.Circuits_dateOfRace = date;

            c.Circuits_direction = properties[3];

            int.TryParse(properties[4], out int laps);
            c.Circuits_laps = laps;

            int.TryParse(properties[5], out int points);
            c.Circuits_distance = points;

            int.TryParse(properties[6], out int pilotWon);
            c.Circuits_pilotWon = pilotWon;

            int.TryParse(properties[7], out int pilotRecord);
            c.Circuits_pilotRecord = pilotRecord;

            this.circuitRepo.AddOne(c);
        }

        /// <summary>
        /// Removes the selected pilot from the table.
        /// </summary>
        /// <param name="id">ID of the pilot that need to be removed.</param>
        /// <returns>Returns if the ID is found.</returns>
        public bool RemoveCircuit(int id)
        {
            var circuitQuery = this.circuitRepo.GetAll().Select(x => x).Where(x => x.Circuits_id == id).ToArray();
            if (circuitQuery != null)
            {
                this.circuitRepo.RemoveOne(circuitQuery[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update pilot data.
        /// </summary>
        /// <param name="id">The ID of the pilot we want to change.</param>
        /// <param name="properties">The refreshed properties that changes the original.</param>
        public void UpdateCircuit(int id, IList<string> properties)
        {
            var circuitQuery = this.circuitRepo.GetAll().Select(x => x).Where(x => x.Circuits_id == id).ToArray();
            Circuits c = new Circuits();

            int.TryParse(properties[0], out int cid);
            c.Circuits_id = cid;

            c.Circuits_name = properties[1];

            DateTime.TryParseExact(properties[2], "yyyy.M.d", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime date);
            c.Circuits_dateOfRace = date;

            c.Circuits_direction = properties[3];

            int.TryParse(properties[4], out int laps);
            c.Circuits_laps = laps;

            int.TryParse(properties[5], out int points);
            c.Circuits_distance = points;

            if (properties[6] != string.Empty)
            {
                int.TryParse(properties[6], out int pilotWon);
                c.Circuits_pilotWon = pilotWon;
            }
            else
            {
                c.Circuits_pilotWon = null;
            }

            if (properties[7] != string.Empty)
            {
                int.TryParse(properties[7], out int pilotRecord);
                c.Circuits_pilotRecord = pilotRecord;
            }
            else
            {
                c.Circuits_pilotRecord = null;
            }

            this.circuitRepo.UpdateOne(circuitQuery[0], c);
        }
    }
}
