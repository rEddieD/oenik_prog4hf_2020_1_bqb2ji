﻿// <copyright file="JavaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Person class.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Name of person.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ID of person.
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Value of person.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Parse the xml.
        /// </summary>
        /// <param name="node">Current node.</param>
        /// <returns>Person object.</returns>
        public static Person Parse(XElement node)
        {
            return new Person()
            {
                Name = node.Element("desc")?.Value,
                ID = int.Parse(node.Element("id")?.Value),
                Value = int.Parse(node.Element("value")?.Value),
            };
        }
    }
}
