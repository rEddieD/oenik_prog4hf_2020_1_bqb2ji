﻿// <copyright file="IPilotLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using Formula1.Data;
    using Formula1.Repository;

    /// <summary>
    /// The required methods for Logic of pilot table.
    /// </summary>
    public interface IPilotLogic
    {
        /// <summary>
        /// Get one pilot by ID.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <returns>Returns one pilot.</returns>
        List<string> GetOnePilot(int id);

        /// <summary>
        /// Get one pilot by ID as pilot.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <returns>Returns one pilot.</returns>
        Pilots GetOnePilotAsPilot(int id);

        /// <summary>
        /// Gets all the pilots of the table.
        /// </summary>
        /// <returns>Returns all the pilots.</returns>
        List<List<string>> GetAllPilots();

        /// <summary>
        /// Gets all the pilots of the table as Pilots.
        /// </summary>
        /// <returns>Returns all the pilots.</returns>
        IQueryable<Pilots> GetAllPilotsAsPilots();

        /// <summary>
        /// Creates one pilot in the database.
        /// </summary>
        /// <param name="p">Pilot entity.</param>
        /// <returns>Returns whether the operation was successful.</returns>
        bool CreateOnePilot(Pilots p);

        /// <summary>
        /// Removes one pilot from the table whose ID matches.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <returns>Returns if it managed to found the ID.</returns>
        bool RemovePilot(int id);

        /// <summary>
        /// Update one pilot in the pilots table.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <param name="properties">The renewed properties.</param>
        void UpdatePilot(int id, IList<string> properties);

        /// <summary>
        /// Update one pilot in the pilots table.
        /// </summary>
        /// <param name="id">Id of pilot.</param>
        /// <param name="p">Pilot entity.</param>
        /// <returns>Returns if it finds ID.</returns>
        bool UpdatePilotAsPilot(int id, Pilots p);

        /// <summary>
        /// Create one pilot in the pilots table.
        /// </summary>
        /// <param name="obj">The Parameters.</param>
        void CreatePilot(IList<string> obj);

        /// <summary>
        /// Change the team of the selected pilot.
        /// </summary>
        /// <param name="id">The ID of the pilot to select.</param>
        /// <param name="pilotTeam">Change the selected pilot's teamID.</param>
        void ChangeTeam(int id, int pilotTeam);

        /// <summary>
        /// Change the points of the selected pilot.
        /// </summary>
        /// <param name="id">The ID of the pilot to select.</param>
        /// <param name="point">Change the selected pilot's points.</param>
        void ChangePoint(int id, int point);
    }
}
