﻿// <copyright file="OldRecordsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Formula1.Data;
    using Formula1.Repository;

    /// <summary>
    /// Business class for Pilots table.
    /// </summary>
    public class OldRecordsLogic
    {
        private IOldRecordsRepository recordRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="OldRecordsLogic"/> class.
        /// </summary>
        public OldRecordsLogic()
        {
            this.recordRepo = new OldRecordsRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OldRecordsLogic"/> class.
        /// </summary>
        /// <param name="repo">Pilot repository.</param>
        public OldRecordsLogic(IOldRecordsRepository repo)
        {
            this.recordRepo = repo;
        }

        /// <summary>
        /// Get all the pilots in the table.
        /// </summary>
        /// <returns>Returns all the pilots in table.</returns>
        public List<List<string>> GetAllRecords()
        {
            var allRecords = this.recordRepo.GetAll();
            List<List<string>> ret = new List<List<string>>(allRecords.AsEnumerable().Select(
                x => new List<string>()
                {
                    x.OldRecords_id.ToString(),
                    x.OldRecords_name,
                    x.OldRecords_year.ToString(),
                    x.OldRecords_recordTime.ToString(),
                }));
            return ret;
        }

        /// <summary>
        /// Get one pilot.
        /// </summary>
        /// <param name="id">ID of the pilot.</param>
        /// <returns>Returns the pilot with the ID.</returns>
        public List<string> GetOneRecord(int id)
        {
            var oneRecord = this.recordRepo.GetOne(id);
            List<string> ret = new List<string>()
            {
                oneRecord.OldRecords_id.ToString(),
                oneRecord.OldRecords_name,
                oneRecord.OldRecords_year.ToString(),
                oneRecord.OldRecords_recordTime.ToString(),
            };
            return ret;
        }

        /// <summary>
        /// Creating a pilot object and passing it to the repository.
        /// </summary>
        /// <param name="properties">List of properties a pilot has.</param>
        public void CreateRecord(IList<string> properties)
        {
            OldRecords r = new OldRecords();

            int.TryParse(properties[0], out int id);
            r.OldRecords_id = id;

            r.OldRecords_name = properties[1];

            int.TryParse(properties[2], out int date);
            r.OldRecords_year = date;

            TimeSpan.TryParseExact(properties[3], "mm:ss.fff", CultureInfo.CurrentCulture, out TimeSpan time);
            r.OldRecords_recordTime = time;

            this.recordRepo.AddOne(r);
        }

        /// <summary>
        /// Removes the selected pilot from the table.
        /// </summary>
        /// <param name="id">ID of the pilot that need to be removed.</param>
        /// /// <returns>Returns if the ID is found.</returns>
        public bool RemoveRecord(int id)
        {
            var recordQuery = this.recordRepo.GetAll().Select(x => x).Where(x => x.OldRecords_id == id).ToArray();
            if (recordQuery != null)
            {
                this.recordRepo.RemoveOne(recordQuery[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update pilot data.
        /// </summary>
        /// <param name="id">The ID of the pilot we want to change.</param>
        /// <param name="properties">The refreshed properties that changes the original.</param>
        public void UpdateRecord(int id, IList<string> properties)
        {
            var circuitQuery = this.recordRepo.GetAll().Select(x => x).Where(x => x.OldRecords_id == id).ToArray();
            OldRecords r = new OldRecords();

            int.TryParse(properties[0], out int rid);
            r.OldRecords_id = rid;

            r.OldRecords_name = properties[1];

            int.TryParse(properties[2], out int date);
            r.OldRecords_year = date;

            TimeSpan.TryParseExact(properties[3], "mm:ss.fff", CultureInfo.CurrentCulture, out TimeSpan time);
            r.OldRecords_recordTime = time;

            this.recordRepo.UpdateOne(circuitQuery[0], r);
        }
    }
}
