﻿// <copyright file="JavaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Class for java endpoint.
    /// </summary>
    public class JavaLogic
    {
        private static WebClient wc = new WebClient();
        private string web = @"http://localhost:8080/PilotProject/FirstServlet?times=";

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaLogic"/> class.
        /// </summary>
        /// <param name="paramValue">Parameter added to the URL.</param>
        public JavaLogic(int paramValue)
        {
            StringBuilder sb = new StringBuilder(this.web);
            sb.Append(paramValue.ToString());
            this.web = sb.ToString();
        }

        /// <summary>
        /// Parse the xml to a string.
        /// </summary>
        /// <returns>Returns the xml elements.</returns>
        public IEnumerable<Person> GetString()
        {
            XDocument xDoc = XDocument.Load(this.web);
            var q = xDoc.Root.Elements("element").Select(x => Person.Parse(x));
            return q;
        }
    }
}
