﻿// <copyright file="PilotLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Formula1.Data;
    using Formula1.Repository;

    /// <summary>
    /// Business class for Pilots table.
    /// </summary>
    public class PilotLogic : IPilotLogic
    {
        private IPilotRepository pilotRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PilotLogic"/> class.
        /// </summary>
        public PilotLogic()
        {
            this.pilotRepo = new PilotRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PilotLogic"/> class.
        /// </summary>
        /// <param name="repo">Pilot repository.</param>
        public PilotLogic(IPilotRepository repo)
        {
            this.pilotRepo = repo;
        }

        /// <summary>
        /// Change point of a specific pilot.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <param name="point">The number of points of the pilot.</param>
        public void ChangePoint(int id, int point)
        {
            this.pilotRepo.ChangePoint(id, point);
        }

        /// <summary>
        /// Change the team of a pilot.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <param name="pilotTeam">The ID of the team.</param>
        public void ChangeTeam(int id, int pilotTeam)
        {
            this.pilotRepo.ChangeTeam(id, pilotTeam);
        }

        /// <summary>
        /// Get all the pilots in the table.
        /// </summary>
        /// <returns>Returns all the pilots in table.</returns>
        public List<List<string>> GetAllPilots()
        {
            var allPilots = this.pilotRepo.GetAll();
            List<List<string>> ret = new List<List<string>>(allPilots.AsEnumerable().Select(
                x => new List<string>()
                {
                    x.pilot_id.ToString(),
                    x.pilot_name,
                    x.pilot_birth.ToString(),
                    x.pilot_nationality,
                    x.pilot_points.ToString(),
                    x.pilot_racesWon.ToString(),
                    x.pilot_team.ToString(),
                }));
            return ret;
        }

        /// <summary>
        /// Get all the pilots in the table as Pilots enumerable.
        /// </summary>
        /// <returns>Returns all the pilots in table.</returns>
        public IQueryable<Pilots> GetAllPilotsAsPilots()
        {
            return this.pilotRepo.GetAll();
        }

        /// <summary>
        /// Get one pilot.
        /// </summary>
        /// <param name="id">ID of the pilot.</param>
        /// <returns>Returns the pilot with the ID.</returns>
        public List<string> GetOnePilot(int id)
        {
            var onePilot = this.pilotRepo.GetOne(id);
            List<string> ret = new List<string>()
            {
                onePilot.pilot_id.ToString(),
                onePilot.pilot_name,
                onePilot.pilot_birth.ToString(),
                onePilot.pilot_nationality,
                onePilot.pilot_points.ToString(),
                onePilot.pilot_racesWon.ToString(),
                onePilot.pilot_team.ToString(),
            };
            return ret;
        }

        /// <summary>
        /// Get one pilot.
        /// </summary>
        /// <param name="id">ID of the pilot.</param>
        /// <returns>Returns the pilot with the ID.</returns>
        public Pilots GetOnePilotAsPilot(int id)
        {
            return this.pilotRepo.GetOne(id);
        }

        /// <summary>
        /// Creating a pilot object and passing it to the repository.
        /// </summary>
        /// <param name="properties">List of properties a pilot has.</param>
        public void CreatePilot(IList<string> properties)
        {
            Pilots p = new Pilots();

            int.TryParse(properties[0], out int id);
            p.pilot_id = id;

            p.pilot_name = properties[1];

            DateTime.TryParseExact(properties[2], "yyyy.M.d", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime birth);
            p.pilot_birth = birth;

            p.pilot_nationality = properties[3];

            int.TryParse(properties[4], out int points);
            p.pilot_points = points;

            int.TryParse(properties[5], out int raceWon);
            p.pilot_racesWon = raceWon;

            int.TryParse(properties[6], out int teamID);
            p.pilot_team = teamID;

            this.pilotRepo.AddOne(p);
        }

        /// <summary>
        /// Creating a pilot object and passing it to the repository.
        /// </summary>
        /// <param name="properties">List of properties a pilot has.</param>
        /// <returns>Return whether the operation was successful.</returns>
        public bool CreateOnePilot(Pilots p)
        {
            var exist = this.pilotRepo.GetAll().Select(x => x).Where(x => x.pilot_id == p.pilot_id).ToArray();
            if (exist.Length == 0)
            {
                this.pilotRepo.AddOne(p);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes the selected pilot from the table.
        /// </summary>
        /// <param name="id">ID of the pilot that need to be removed.</param>
        /// <returns>Returns if it found the sought value.</returns>
        public bool RemovePilot(int id)
        {
            var pilotQuery = this.pilotRepo.GetAll().Select(x => x).Where(x => x.pilot_id == id).ToArray();
            if (pilotQuery != null)
            {
                this.pilotRepo.RemoveOne(pilotQuery[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update pilot data.
        /// </summary>
        /// <param name="id">The ID of the pilot we want to change.</param>
        /// <param name="properties">The refreshed properties that changes the original.</param>
        public void UpdatePilot(int id, IList<string> properties)
        {
            var pilotQuery = this.pilotRepo.GetAll().Select(x => x).Where(x => x.pilot_id == id).ToArray();
            Pilots p = new Pilots();

            p.pilot_name = properties[1];

            DateTime.TryParseExact(properties[2], "yyyy.M.d", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime birth);
            p.pilot_birth = birth;

            p.pilot_nationality = properties[3];

            int.TryParse(properties[4], out int points);
            p.pilot_points = points;

            int.TryParse(properties[5], out int raceWon);
            p.pilot_racesWon = raceWon;

            int.TryParse(properties[6], out int teamID);
            p.pilot_team = teamID;

            this.pilotRepo.UpdateOne(pilotQuery[0], p);
        }

       /// <summary>
       /// Update pilot data.
       /// </summary>
       /// <param name="id">Id of pilots.</param>
       /// <param name="p">Pilot entity.</param>
       /// <returns>Returns whether successful.</returns>
        public bool UpdatePilotAsPilot(int id, Pilots p)
        {
            var pilotQuery = this.pilotRepo.GetAll().Select(x => x).Where(x => x.pilot_id == id).ToArray();
            if (pilotQuery != null)
            {
                this.pilotRepo.UpdateOne(pilotQuery[0], p);
                return true;
            }
            return false;
        }
    }
}
