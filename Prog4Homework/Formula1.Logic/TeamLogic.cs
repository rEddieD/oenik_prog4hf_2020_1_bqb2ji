﻿// <copyright file="TeamLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Formula1.Data;
    using Formula1.Repository;

    /// <summary>
    /// Business class for Pilots table.
    /// </summary>
    public class TeamLogic
    {
        private ITeamRepository teamRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamLogic"/> class.
        /// </summary>
        public TeamLogic()
        {
            this.teamRepo = new TeamRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamLogic"/> class.
        /// </summary>
        /// <param name="repo">Pilot repository.</param>
        public TeamLogic(ITeamRepository repo)
        {
            this.teamRepo = repo;
        }

        /// <summary>
        /// Get all the pilots in the table.
        /// </summary>
        /// <returns>Returns all the pilots in table.</returns>
        public List<List<string>> GetAllTeams()
        {
            var allTeams = this.teamRepo.GetAll();
            List<List<string>> ret = new List<List<string>>(allTeams.AsEnumerable().Select(
                x => new List<string>()
                {
                    x.team_id.ToString(),
                    x.team_name,
                    x.team_nationality,
                    x.team_teamChief,
                    x.team_powerUnit,
                    x.team_budget.ToString(),
                    x.team_employees.ToString(),
                }));
            return ret;
        }

        /// <summary>
        /// Get one pilot.
        /// </summary>
        /// <param name="id">ID of the pilot.</param>
        /// <returns>Returns the pilot with the ID.</returns>
        public List<string> GetOneTeam(int id)
        {
            var oneTeam = this.teamRepo.GetOne(id);
            List<string> ret = new List<string>()
            {
                oneTeam.team_id.ToString(),
                oneTeam.team_name,
                oneTeam.team_nationality,
                oneTeam.team_teamChief,
                oneTeam.team_powerUnit,
                oneTeam.team_budget.ToString(),
                oneTeam.team_employees.ToString(),
            };
            return ret;
        }

        /// <summary>
        /// Creating a pilot object and passing it to the repository.
        /// </summary>
        /// <param name="properties">List of properties a pilot has.</param>
        public void CreateTeam(IList<string> properties)
        {
            Teams t = new Teams();

            int.TryParse(properties[0], out int cid);
            t.team_id = cid;

            t.team_name = properties[1];

            t.team_nationality = properties[2];

            t.team_teamChief = properties[3];

            t.team_powerUnit = properties[4];

            int.TryParse(properties[5], out int employees);
            t.team_employees = employees;

            int.TryParse(properties[6], out int budget);
            t.team_budget = budget;

            this.teamRepo.AddOne(t);
        }

        /// <summary>
        /// Removes the selected pilot from the table.
        /// </summary>
        /// <param name="id">ID of the pilot that need to be removed.</param>
        /// <returns>Returns if the ID is found.</returns>
        public bool RemoveTeam(int id)
        {
            var teamQuery = this.teamRepo.GetAll().Select(x => x).Where(x => x.team_id == id).ToArray();
            if (teamQuery != null)
            {
                this.teamRepo.RemoveOne(teamQuery[0]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Update pilot data.
        /// </summary>
        /// <param name="id">The ID of the pilot we want to change.</param>
        /// <param name="properties">The refreshed properties that changes the original.</param>
        public void UpdateTeam(int id, IList<string> properties)
        {
            var teamQuery = this.teamRepo.GetAll().Select(x => x).Where(x => x.team_id == id).ToArray();
            Teams t = new Teams();

            int.TryParse(properties[0], out int cid);
            t.team_id = cid;

            t.team_name = properties[1];

            t.team_nationality = properties[2];

            t.team_teamChief = properties[3];

            t.team_powerUnit = properties[4];

            int.TryParse(properties[5], out int employees);
            t.team_employees = employees;

            int.TryParse(properties[6], out int budget);
            t.team_budget = budget;

            this.teamRepo.UpdateOne(teamQuery[0], t);
        }
    }
}