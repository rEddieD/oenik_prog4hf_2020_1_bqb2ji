﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Formula1.ConsoleClient
{
    class Program
    {
        public class Pilot
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public int Birth { get; set; }
            public int Points { get; set; }
            public override string ToString()
            {
                return $"{ID}\t{Name}\t{Birth}\t{Points}";
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:56586/api/PilotApi/";

            using(HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Pilot>>(json);
                foreach(var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Pilot.Name), "Palik László");
                postData.Add(nameof(Pilot.Birth), "1962");
                postData.Add(nameof(Pilot.Points), "99");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int pilotId = JsonConvert.DeserializeObject<List<Pilot>>(json).Single(x => x.Name == "Palik László").ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Pilot.ID), pilotId.ToString());
                postData.Add(nameof(Pilot.Name), "Palik László");
                postData.Add(nameof(Pilot.Birth), "1962");
                postData.Add(nameof(Pilot.Points), "199");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + pilotId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
