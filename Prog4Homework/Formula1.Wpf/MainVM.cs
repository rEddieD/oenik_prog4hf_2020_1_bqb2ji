﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Formula1.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private PilotVM selectedPilot;
		private ObservableCollection<PilotVM> allPilots;

		public PilotVM SelectedPilot
		{
			get { return selectedPilot; }
			set { Set(ref selectedPilot, value); }
		}

		public ObservableCollection<PilotVM> AllPilots
		{
			get { return allPilots; }
			set { Set(ref allPilots, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<PilotVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();
			DelCmd = new RelayCommand(() => logic.ApiDelPilot(selectedPilot));
			AddCmd = new RelayCommand(() => logic.EditPilot(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditPilot(selectedPilot, EditorFunc));
			LoadCmd = new RelayCommand(() => AllPilots = new ObservableCollection<PilotVM>(logic.ApiGetPilots()));
		}

	}
}
