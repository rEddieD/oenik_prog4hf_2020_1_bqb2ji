﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formula1.Wpf
{
    class PilotVM : ObservableObject
    {
        private int id;
        private string name;
        private int birth;
        private int points;
        private string nationality;

        public int ID
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public int Birth
        {
            get { return birth; }
            set { Set(ref birth, value); }
        }

        public int Points
        {
            get { return points; }
            set { Set(ref points, value); }
        }

        public string Nationality
        {
            get { return nationality; }
            set { Set(ref nationality, value); }
        }

        public void CopyFrom(PilotVM other)
        {
            if (other == null)
                return;
            this.ID = other.ID;
            this.Name = other.Name;
            this.Birth = other.Birth;
            this.Points = other.Points;
            this.Nationality = other.Nationality;
        }
    }
}
