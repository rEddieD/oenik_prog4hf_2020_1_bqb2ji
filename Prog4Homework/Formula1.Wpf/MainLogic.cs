﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace Formula1.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:56586/api/PilotApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully." : "Operation failed.";
            Messenger.Default.Send(msg, "PilotResult");
        }

        public List<PilotVM> ApiGetPilots()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<PilotVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelPilot(PilotVM pilot)
        {
            bool success = false;
            if(pilot != null)
            {
                string json = client.GetStringAsync(url + "del/" + pilot.ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        private bool ApiEditPilot(PilotVM pilot, bool isEditing)
        {

            if (pilot == null)
            {
                return false;
            }
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(PilotVM.ID), pilot.ID.ToString());
            }
            postData.Add(nameof(PilotVM.Name), pilot.Name.ToString());
            postData.Add(nameof(PilotVM.Birth), pilot.Birth.ToString());
            postData.Add(nameof(PilotVM.Points), pilot.Points.ToString());
            postData.Add(nameof(PilotVM.Nationality), pilot.Nationality.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditPilot(PilotVM pilot, Func<PilotVM, bool> editor)
        {
            PilotVM clone = new PilotVM();
            if (pilot != null)
            {
                clone.CopyFrom(pilot);
            }
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (pilot != null)
                    success = ApiEditPilot(clone, true);
                else
                    success = ApiEditPilot(clone, false);
            }
            SendMessage(success == true);
        }

    }
}
