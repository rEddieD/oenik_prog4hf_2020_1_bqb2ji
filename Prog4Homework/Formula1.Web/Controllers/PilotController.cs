﻿using AutoMapper;
using Formula1.Data;
using Formula1.Logic;
using Formula1.Repository;
using Formula1.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Formula1.Web.Controllers
{
    public class PilotController : Controller
    {
        IPilotLogic logic;
        IMapper mapper;
        PilotViewModel vm;
        public PilotController()
        {
            PilotRepository pilotRepo = new PilotRepository();
            logic = new PilotLogic(pilotRepo);
            mapper = MapperFactory.CreateMapper();

            vm = new PilotViewModel();
            vm.EditedPilot = new Pilot();
            var pilots = logic.GetAllPilotsAsPilots().ToList();
            vm.ListOfPilots = mapper.Map<IList<Data.Pilots>, List<Models.Pilot>>(pilots);
        }

        private Pilot GetPilotModel(int id)
        {
            Pilots onePilot = logic.GetOnePilotAsPilot(id);
            return mapper.Map<Data.Pilots, Models.Pilot>(onePilot);
        }

        // GET: Pilot
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("PilotIndex", vm);
        }

        // GET: Pilot/Details/5
        public ActionResult Details(int id)
        {
            return View("PilotDetails", GetPilotModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.RemovePilot(id))
                TempData["editResult"] = "DELETE OK";
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedPilot = GetPilotModel(id);
            return View("PilotIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Pilot pilot, string editAction)
        {
            if(ModelState.IsValid && pilot != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    DateTime.TryParseExact(pilot.Birth.ToString(), "yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime birth);
                    Pilots p = new Pilots
                    {
                        pilot_id = pilot.ID,
                        pilot_name = pilot.Name,
                        pilot_birth = birth,
                        pilot_points = pilot.Points,
                        pilot_racesWon = pilot.RacesWon,
                        pilot_nationality = pilot.Nationality,
                        pilot_team = null
                    };
                    logic.CreateOnePilot(p);
                }
                else
                {
                    DateTime.TryParseExact(pilot.Birth.ToString(), "yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime birth);
                    Pilots p = new Pilots
                    {
                        pilot_id = pilot.ID,
                        pilot_name = pilot.Name,
                        pilot_birth = birth,
                        pilot_points = pilot.Points,
                        pilot_racesWon = pilot.RacesWon,
                        pilot_nationality = pilot.Nationality,
                        pilot_team = null
                    };
                    bool success = logic.UpdatePilotAsPilot(pilot.ID, p);
                    if (!success) TempData["editResult"] = "Edit FAIL";
                }
                return RedirectToAction(nameof(Index));
            }
            else 
            {
                ViewData["editAction"] = "Edit";
                vm.EditedPilot = pilot;
                return View("PilotIndex", vm);
            }
        }
    }
}
