﻿using AutoMapper;
using Formula1.Data;
using Formula1.Logic;
using Formula1.Repository;
using Formula1.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Formula1.Web.Controllers
{
    public class PilotApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IPilotLogic pilotLogic;
        IMapper mapper;

        public PilotApiController()
        {
            PilotRepository pilotRepo = new PilotRepository();
            pilotLogic = new PilotLogic(pilotRepo);
            mapper = MapperFactory.CreateMapper();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Pilot> GetAll()
        {
            var pilots = pilotLogic.GetAllPilotsAsPilots();
            return mapper.Map<IQueryable<Data.Pilots>, List<Models.Pilot>>(pilots);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOnePilot(int id)
        {
            bool success = pilotLogic.RemovePilot(id);
            return new ApiResult { OperationResult = success };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOnePilot(Pilot pilot)
        {
            DateTime.TryParseExact(pilot.Birth.ToString(), "yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime birth);
            Pilots p = new Pilots
            {
                pilot_id = pilot.ID,
                pilot_name = pilot.Name,
                pilot_birth = birth,
                pilot_points = pilot.Points,
                pilot_racesWon = pilot.RacesWon,
                pilot_nationality = pilot.Nationality,
                pilot_team = null
            };
            pilotLogic.CreateOnePilot(p);
            return new ApiResult { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOnePilot(Pilot pilot)
        {
            DateTime.TryParseExact(pilot.Birth.ToString(), "yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime birth);
            Pilots p = new Pilots
            {
                pilot_id = pilot.ID,
                pilot_name = pilot.Name,
                pilot_birth = birth,
                pilot_points = pilot.Points,
                pilot_racesWon = pilot.RacesWon,
                pilot_nationality = pilot.Nationality,
                pilot_team = null
            };
            bool success = pilotLogic.UpdatePilotAsPilot(pilot.ID, p);
            return new ApiResult { OperationResult = success };
        }
    }
}
