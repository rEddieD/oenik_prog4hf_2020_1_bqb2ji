﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Formula1.Web.Models
{
    public class PilotViewModel
    {
        public Pilot EditedPilot { get; set; }
        public List<Pilot> ListOfPilots { get; set; }
    }
}