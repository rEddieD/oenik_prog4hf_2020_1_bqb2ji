﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Formula1.Web.Models
{
    public class Pilot
    {
        [Display(Name = "Pilot ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Pilot Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Pilot Birth")]
        [Required]
        public int Birth { get; set; }

        [Display(Name = "Pilot Points")]
        [Required]
        public int Points { get; set; }

        [Display(Name = "Pilot Races Won")]
        [Required]
        public int RacesWon { get; set; }

        [Display(Name = "Pilot Nationality")]
        [Required]
        public string Nationality { get; set; }

        [Display(Name = "Pilot Team")]
        public string Team { get; set; }
    }
}