﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Formula1.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Formula1.Data.Pilots, Formula1.Web.Models.Pilot>().
                ForMember(dest => dest.ID, map => map.MapFrom(src => src.pilot_id)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.pilot_name)).
                ForMember(dest => dest.Birth, map => map.MapFrom(src => src.pilot_birth.Value.Year)).
                ForMember(dest => dest.Points, map => map.MapFrom(src => src.pilot_points)).
                ForMember(dest => dest.RacesWon, map => map.MapFrom(src => src.pilot_racesWon)).
                ForMember(dest => dest.Nationality, map => map.MapFrom(src => src.pilot_nationality)).
                ForMember(dest => dest.Team, map => map.MapFrom(src => src.pilot_team));
            });

            return config.CreateMapper();
        }
    }
}