﻿// <copyright file="IOldRecordsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using Formula1.Data;

    /// <summary>
    /// Interface for OldRecords.
    /// </summary>
    public interface IOldRecordsRepository : IRepository<OldRecords>
    {
    }
}
