﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using System.Linq;

    /// <summary>
    /// Interface for all the tables.
    /// </summary>
    /// <typeparam name="T"> The type of table we expect.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Get one record from the database.
        /// </summary>
        /// <param name="id">The ID of the member of the database.</param>
        /// <returns>Returns the record with that ID.</returns>
        T GetOne(int id);

        /// <summary>
        /// Gets all of the records from the database.
        /// </summary>
        /// <returns>Returns all of the records of a database.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Remove one record from the table.
        /// </summary>
        /// <param name="entity">The entity of the table that we want to remove.</param>
        void RemoveOne(T entity);

        /// <summary>
        /// Update one record in the table.
        /// </summary>
        /// <param name="oldEntity">The entity of the table that we want to update.</param>
        /// <param name="newEntity">The entity of the table that we want to update with.</param>
        void UpdateOne(T oldEntity, T newEntity);

        /// <summary>
        /// Add one entry in the table.
        /// </summary>
        /// <param name="entity">The new entry.</param>
        void AddOne(T entity);
    }
}
