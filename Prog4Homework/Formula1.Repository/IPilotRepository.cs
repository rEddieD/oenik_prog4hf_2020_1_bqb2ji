﻿// <copyright file="IPilotRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using Formula1.Data;

    /// <summary>
    /// Interface for the Pilots table.
    /// </summary>
    public interface IPilotRepository : IRepository<Pilots>
    {
        /// <summary>
        /// Change the team of a pilot by ID.
        /// </summary>
        /// <param name="id">ID of a pilot.</param>
        /// <param name="pilotTeam">New team of the pilot.</param>
        void ChangeTeam(int id, int pilotTeam);

        /// <summary>
        /// Change points of a pilot.
        /// </summary>
        /// <param name="id">ID of a pilot.</param>
        /// <param name="point">The new point value of a pilot.</param>
        void ChangePoint(int id, int point);
    }
}
