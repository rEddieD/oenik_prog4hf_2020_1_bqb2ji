﻿// <copyright file="ICircuitRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using Formula1.Data;

    /// <summary>
    /// Interface for the Circuits table.
    /// </summary>
    public interface ICircuitRepository : IRepository<Circuits>
    {
        /// <summary>
        /// Change the current distance of a track.
        /// </summary>
        /// <param name="id">The ID of the track.</param>
        /// <param name="distance">The length of a track.</param>
        void ChangeDistance(int id, int distance);
    }
}
