﻿// <copyright file="CircuitRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using System.Linq;
    using Formula1.Data;

    /// <summary>
    /// Repository of circuit table.
    /// </summary>
    public class CircuitRepository : Repository<Circuits>, ICircuitRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CircuitRepository"/> class.
        /// </summary>
        public CircuitRepository()
            : base(new Formula1DatabaseEntities())
        {
        }

        /// <summary>
        /// Search for a single element by ID.
        /// </summary>
        /// <param name="id">The ID of the circuit.</param>
        /// <returns>Returns the record corresponding to the ID.</returns>
        public override Circuits GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Circuits_id == id);
        }

        /// <summary>
        /// Change the length of a track.
        /// </summary>
        /// <param name="id">The ID of the track.</param>
        /// <param name="distance">The length of the track.</param>
        public void ChangeDistance(int id, int distance)
        {
            this.GetOne(id).Circuits_distance = distance;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Remove one circuit from the Circuits table.
        /// </summary>
        /// <param name="circuit">One circuit that we want to remove from the table.</param>
        public override void RemoveOne(Circuits circuit)
        {
            Formula1DatabaseEntities circuitContext = this.Context as Formula1DatabaseEntities;
            circuitContext.Circuits.Remove(circuit);
            circuitContext.SaveChanges();
        }

        /// <summary>
        /// Updates a circuit in the Circuits table. (By removing the original entry and adding a new entry.)
        /// </summary>
        /// <param name="originalCircuit">The original entry.</param>
        /// <param name="newCircuit">The new entry.</param>
        public override void UpdateOne(Circuits originalCircuit, Circuits newCircuit)
        {
            Formula1DatabaseEntities circuitContext = this.Context as Formula1DatabaseEntities;
            circuitContext.Circuits.Remove(originalCircuit);
            circuitContext.Circuits.Add(newCircuit);
            circuitContext.SaveChanges();
        }

        /// <summary>
        /// Adds a circuit to the Circuits table.
        /// </summary>
        /// <param name="circuit">The new circuit.</param>
        public override void AddOne(Circuits circuit)
        {
            Formula1DatabaseEntities circuitContext = this.Context as Formula1DatabaseEntities;
            circuitContext.Circuits.Add(circuit);
            circuitContext.SaveChanges();
        }
    }
}
