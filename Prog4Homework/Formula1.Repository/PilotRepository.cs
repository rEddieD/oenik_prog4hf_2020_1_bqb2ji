﻿// <copyright file="PilotRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using System.Collections;
    using System.Data;
    using System.Linq;
    using Formula1.Data;

    /// <summary>
    /// Repository of the pilots table.
    /// </summary>
    public class PilotRepository : Repository<Pilots>, IPilotRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PilotRepository"/> class.
        /// </summary>
        public PilotRepository()
            : base(new Formula1DatabaseEntities())
        {
        }

        /// <summary>
        /// Search for a single element by ID.
        /// </summary>
        /// <param name="id">The ID of the sought record.</param>
        /// <returns>Returns the record with the corresponding ID.</returns>
        public override Pilots GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.pilot_id == id);
        }

        /// <summary>
        /// Change the number of points of a pilot.
        /// </summary>
        /// <param name="id">The ID of the driver.</param>
        /// <param name="point">The number of points. </param>
        public void ChangePoint(int id, int point)
        {
            this.GetOne(id).pilot_points = point;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Change the team of a pilot.
        /// </summary>
        /// <param name="id">The ID of the pilot.</param>
        /// <param name="pilotTeam">The ID of the team.</param>
        public void ChangeTeam(int id, int pilotTeam)
        {
            this.GetOne(id).pilot_team = pilotTeam;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Remove one pilot from the Pilots table.
        /// </summary>
        /// <param name="pilot">One pilot that we want to remove from the table.</param>
        public override void RemoveOne(Pilots pilot)
        {
            Formula1DatabaseEntities pilotContext = this.Context as Formula1DatabaseEntities;
            pilotContext.Pilots.Remove(pilot);
            pilotContext.SaveChanges();
        }

        /// <summary>
        /// Updates a pilot in the Pilots table. (By removing the original entry and adding a new entry.)
        /// </summary>
        /// <param name="originalPilot">The original entry.</param>
        /// <param name="newPilot">The new entry.</param>
        public override void UpdateOne(Pilots originalPilot, Pilots newPilot)
        {
            Formula1DatabaseEntities pilotContext = this.Context as Formula1DatabaseEntities;
            pilotContext.Pilots.Remove(originalPilot);
            pilotContext.Pilots.Add(newPilot);
            pilotContext.SaveChanges();
        }

        /// <summary>
        /// Adds a pilot to the Pilots table.
        /// </summary>
        /// <param name="pilot">The new pilot.</param>
        public override void AddOne(Pilots pilot)
        {
            Formula1DatabaseEntities pilotContext = this.Context as Formula1DatabaseEntities;
            pilotContext.Pilots.Add(pilot);
            pilotContext.SaveChanges();
        }
    }
}
