﻿// <copyright file="TeamRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using System;
    using System.Linq;
    using Formula1.Data;

    /// <summary>
    /// Repository for Teams table.
    /// </summary>
    public class TeamRepository : Repository<Teams>, ITeamRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TeamRepository"/> class.
        /// </summary>
        public TeamRepository()
            : base(new Formula1DatabaseEntities())
        {
        }

        /// <summary>
        /// Search for a single element by ID.
        /// </summary>
        /// <param name="id">The ID of the sought record.</param>
        /// <returns>Returns the record with the corresponding ID.</returns>
        public override Teams GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.team_id == id);
        }

        /// <summary>
        /// Remove one team from the Teams table.
        /// </summary>
        /// <param name="team">One team that we want to remove from the table.</param>
        public override void RemoveOne(Teams team)
        {
            Formula1DatabaseEntities teamContext = this.Context as Formula1DatabaseEntities;
            teamContext.Teams.Remove(team);
            teamContext.SaveChanges();
        }

        /// <summary>
        /// Updates a team in the Teams table. (By removing the original entry and adding a new entry.)
        /// </summary>
        /// <param name="originalTeam">The original entry.</param>
        /// <param name="newTeam">The new entry.</param>
        public override void UpdateOne(Teams originalTeam, Teams newTeam)
        {
            Formula1DatabaseEntities teamContext = this.Context as Formula1DatabaseEntities;
            teamContext.Teams.Remove(originalTeam);
            teamContext.Teams.Add(newTeam);
            teamContext.SaveChanges();
        }

        /// <summary>
        /// Adds a team to the Teams table.
        /// </summary>
        /// <param name="team">The new team.</param>
        public override void AddOne(Teams team)
        {
            Formula1DatabaseEntities teamContext = this.Context as Formula1DatabaseEntities;
            teamContext.Teams.Add(team);
            teamContext.SaveChanges();
        }
    }
}
