﻿// <copyright file="OldRecordsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using System.Linq;
    using Formula1.Data;

    /// <summary>
    /// Repository of circuit table.
    /// </summary>
    public class OldRecordsRepository : Repository<OldRecords>, IOldRecordsRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OldRecordsRepository"/> class.
        /// </summary>
        public OldRecordsRepository()
            : base(new Formula1DatabaseEntities())
        {
        }

        /// <summary>
        /// Search for a single element by ID.
        /// </summary>
        /// <param name="id">The ID of the circuit.</param>
        /// <returns>Returns the record corresponding to the ID.</returns>
        public override OldRecords GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.OldRecords_id == id);
        }

        /// <summary>
        /// Remove one record from the OldRecords table.
        /// </summary>
        /// <param name="oldRecords">One record that we want to remove from the table.</param>
        public override void RemoveOne(OldRecords oldRecords)
        {
            Formula1DatabaseEntities recordContext = this.Context as Formula1DatabaseEntities;
            recordContext.OldRecords.Remove(oldRecords);
            recordContext.SaveChanges();
        }

        /// <summary>
        /// Updates a record in the OldRecords table. (By removing the original entry and adding a new entry.)
        /// </summary>
        /// <param name="oldRecord">The original entry.</param>
        /// <param name="newRecord">The new entry.</param>
        public override void UpdateOne(OldRecords oldRecord, OldRecords newRecord)
        {
            Formula1DatabaseEntities recordContext = this.Context as Formula1DatabaseEntities;
            recordContext.OldRecords.Remove(oldRecord);
            recordContext.OldRecords.Add(newRecord);
            recordContext.SaveChanges();
        }

        /// <summary>
        /// Adds a record to the OldRecord table.
        /// </summary>
        /// <param name="record">The new record.</param>
        public override void AddOne(OldRecords record)
        {
            Formula1DatabaseEntities recordContext = this.Context as Formula1DatabaseEntities;
            recordContext.OldRecords.Add(record);
            recordContext.SaveChanges();
        }
    }
}
