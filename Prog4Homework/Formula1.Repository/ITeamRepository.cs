﻿// <copyright file="ITeamRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using Formula1.Data;

    /// <summary>
    /// Interface for the Teams table.
    /// </summary>
    public interface ITeamRepository : IRepository<Teams>
    {
    }
}
