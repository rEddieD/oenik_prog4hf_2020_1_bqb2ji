﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Formula1.Repository
{
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// Generic repository for all tables.
    /// </summary>
    /// <typeparam name="T">T is a type of table.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// DbContext object.
        /// </summary>
        private DbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// Repository constructor. Sets the DbContext object.
        /// </summary>
        /// <param name="ctx">DbContext object.</param>
        public Repository(DbContext ctx)
        {
            this.Context = ctx;
        }

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        protected DbContext Context { get => this.context; set => this.context = value; }

        /// <summary>
        /// Implementation of generic table getter.
        /// </summary>
        /// <returns>Returns a table.</returns>
        public IQueryable<T> GetAll()
        {
            return this.Context.Set<T>();
        }

        /// <summary>
        /// An abstract method that child repositories will implement for searching for an element and giving it back.
        /// </summary>
        /// <param name="id">The ID of the sought record.</param>
        /// <returns>It returns a single record.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// An abstract method that child repositories will implement for removing one element.
        /// </summary>
        /// <param name="entity">The entity of the table we want to remove.</param>
        public abstract void RemoveOne(T entity);

        /// <summary>
        /// An abstract method that child repositories will implement for updating one element.
        /// </summary>
        /// <param name="oldEntity">The entity of the table we want to update.</param>
        /// <param name="newEntity">The entity of the table we want to update with.</param>
        public abstract void UpdateOne(T oldEntity, T newEntity);

        /// <summary>
        /// Adds one entry to the table.
        /// </summary>
        /// <param name="entity">The new entry.</param>
        public abstract void AddOne(T entity);
    }
}
