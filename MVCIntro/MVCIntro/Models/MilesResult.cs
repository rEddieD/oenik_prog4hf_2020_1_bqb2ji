﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCIntro.Models
{
    public class MilesResult
    {
        public double OriginalInput { get; set; }
        public double NumMiles { get; set; }
        public double NumYards { get; set; }
    }
}