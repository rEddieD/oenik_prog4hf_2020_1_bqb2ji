﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCIntro.Models
{
    public class NumberItem
    {
        public int Value { get; set; }
        public bool IsPrime { get; set; }
        public bool IsMax { get; set; }
    }
}