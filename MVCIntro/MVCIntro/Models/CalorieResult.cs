﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCIntro.Models
{
    public class CalorieResult
    {
        public string OriginalName { get; set; }
        public double BurnedCalories { get; set; }
    }
}