﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCIntro.Models
{
    public class CalorieInput
    {
        public string FullName { get; set; }
        public double Weight { get; set; }
        public string SportType { get; set;}
        public double SportLength { get; set; }
    }

    public class Exercise
    {
        public string name;
        public double calorie;
        public Exercise(string name, double calorie)
        {
            this.name = name;
            this.calorie = calorie;
        }
    }
}