﻿using MVCIntro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCIntro.Controllers
{
    public class CalorieCounterController : Controller
    {
        // GET: CalorieCounter
        public ActionResult Counter()
        {
            return View("CalorieInput");
        }

        [HttpPost]
        public ActionResult Counter(CalorieInput input)
        {
            List<Exercise> exercises = new List<Exercise>
            {
                new Exercise("Running", 1000),
                new Exercise("Yoga", 400),
                new Exercise("Pilates", 472),
                new Exercise("Hiking", 700),
                new Exercise("Swimming", 1000),
                new Exercise("Bicycle", 600)
            };

            CalorieResult cr = new CalorieResult();
            
            foreach (var exercise in exercises)
            {
                if (exercise.name == input.SportType)
                {
                    cr.OriginalName = input.FullName;
                    cr.BurnedCalories = (exercise.calorie / 100) * input.Weight / (60.0 / input.SportLength);
                }
            }

            return View("CalorieResult", cr);
        }
    }
}