﻿using MVCIntro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCIntro.Controllers
{
    public class NikDemoController : Controller
    {
        static Random rnd = new Random();
        private bool IsPrime(int num)
        {
            double lim = Math.Sqrt(num);
            for (int i = 2; i <= lim; i++)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        // GET: NikDemo
        public ActionResult Numbers()
        {
            NumberItem[] myArray = new NumberItem[100];
            for (int i = 0; i < myArray.Length; i++)
            {
                int rndNumber = rnd.Next(100, 1000);
                myArray[i] = new NumberItem()
                {
                    Value = rndNumber,
                    IsPrime = IsPrime(rndNumber)
                };
            }
            int maxValue = myArray.Max(x => x.Value);
            myArray.Where(x => x.Value == maxValue).ToList().ForEach(x => x.IsMax = true);

            return View("Numbers", myArray);
        }

        public ActionResult Miles()
        {
            return View("MilesInput");
        }

        [HttpPost]
        public ActionResult Miles(MilesInput input)
        {
            if (input.NumKm < 0)
            {
                TempData["warning"] = "Invalid data.";
                return this.RedirectToAction(nameof(Miles));
            }

            MilesResult mr = new MilesResult()
            {
                OriginalInput = input.NumKm,
                NumYards = input.NumKm * 1000 * 1.09361,
                NumMiles = input.NumKm * 0.621371
            };
            return View("MilesResult", mr);
        }
    }
}